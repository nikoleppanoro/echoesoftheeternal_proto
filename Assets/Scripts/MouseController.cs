﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class MouseController : MonoBehaviour
{
    [Header("Other")]
    public Camera cam;
    public HUD hud;
    public GraphicRaycaster graphicRaycaster;
    public EventSystem eventSystem;
    public HexTileMap hexTileMap;
    public TurnManager turnManager;
    public CameraController cameraController;

    [Header("Selection Info")]
    [SerializeField] Tile currentTile;
    [SerializeField] Tile previousTile;
    [SerializeField] Unit selectedUnit;

    public Tile CurrentTile {
        get => currentTile;
        set {
            currentTile = value;
        }
    }
    public Tile PreviousTile { 
        get => previousTile;
        set {
            previousTile = value;
        }
    }
    public Unit SelectedUnit { 
        get => selectedUnit;
        set => selectedUnit = value;
    }

    private void Start() {

    }

    private void OnEnable() {
        TurnManager.OnTurnEnd += ResetAll;
    }

    private void OnDisable() {
        TurnManager.OnTurnEnd -= ResetAll;
    }

    void Update()
    {
        if (eventSystem.IsPointerOverGameObject()) {
            return;
        }

        // Left click:
        if (Input.GetMouseButtonDown(0)) {
            Ray ray = cam.ScreenPointToRay(Input.mousePosition);
            RaycastHit hit;

            // If we hit something:
            if (Physics.Raycast(ray, out hit)) {
                Tile hitTile = hit.transform.parent.gameObject.GetComponent<Tile>();
                // Hex tile hit:
                if (hitTile != null) {

                    //// Tile selection:
                    // Previous tile:
                    if (CurrentTile != null) {
                        PreviousTile = CurrentTile;
                        PreviousTile.IsSelected = false;
                    }
                    // Set hit tile as current tile:
                    CurrentTile = hit.transform.parent.gameObject.GetComponent<Tile>();
                    CurrentTile.IsSelected = true;

                    // If we hit the same tile:
                    if (CurrentTile == PreviousTile) {
                        if (cameraController.cameraTarget.transform.position != CurrentTile.transform.position) {
                            // Move camera to clicked tile:
                            cameraController.MoveCameraTo(CurrentTile.transform.position);
                            hexTileMap.UpdateTileDistances();
                        }
                        else {
                            ResetSelection();
                            hexTileMap.ResetTileDistances();
                            hexTileMap.ResetAttackRange();
                            InfoPanelUpdate();
                        }
                        return;
                    }
                    //// Unit movement:
                    if (SelectedUnit != null) {
                        // If unit can move:
                        if (hitTile.Distance <= SelectedUnit.MovementCurrent) {
                            // If there is no unit on hit tile, move unit:
                            if (hitTile.UnitOnTop == null) {
                                //// Move camera to new tile:
                                //cameraController.MoveCameraTo(hitTile.transform.position);
                                MoveUnit(hitTile);
                                InfoPanelUpdate();
                                return;
                            }
                            else if (hitTile.UnitOnTop != null) {
                                // Friendly unit: 
                                if(SelectedUnit.Owner == hitTile.UnitOnTop.Owner) {
                                    // Switch places with the other unit if it can move?

                                }
                                // Enemy unit:
                                else if (SelectedUnit.Owner != hitTile.UnitOnTop.Owner) {
                                    // Unit gets "ambushed" and loses it's movement:
                                    SelectedUnit.MovementCurrent = 0;
                                    SelectedUnit.CanAct = false;
                                    MessageManager.Show("Your " + SelectedUnit.scriptableUnitCard.cardName + " was ambushed!");
                                }
                            }
                        }
                    }
                    //// Unit selection:
                    if (CurrentTile.UnitOnTop != null) {
                        hexTileMap.ResetTileDistances();
                        // Is the unit owned by current player?
                        if (CurrentTile.UnitOnTop.Owner == turnManager.CurrentPlayer) {
                            // Select unit if it can still act on this turn:
                            if (CurrentTile.UnitOnTop.CanAct) {
                                SelectedUnit = CurrentTile.UnitOnTop;
                                Debug.Log(SelectedUnit.name + " selected!");
                                // Show movement distances:
                                CurrentTile.ShowDistances();
                                // Show attack range:
                                CurrentTile.ShowAttackRange();
                            }
                        }
                        // Enemy unit:
                        else {
                            // If unit is in attack range and can attack enemy:
                            if (hitTile.IsInAttackRange && PreviousTile.UnitOnTop.CanAct && hitTile.UnitOnTop.IsVisible) {
                                // Attack:
                                PreviousTile.UnitOnTop.Attack(hitTile.UnitOnTop);
                                ResetAll();
                            }
                            // Can't attack:
                            else {
                                hexTileMap.ResetTileDistances();
                                hexTileMap.ResetAttackRange();
                                SelectedUnit = null;
                            }
                        }
                    }
                    else {
                        // No unit on tile:
                        hexTileMap.ResetTileDistances();
                        hexTileMap.ResetAttackRange();
                        SelectedUnit = null;
                    }
                }
                else {
                    Debug.Log("We hit something else (how tho?)");
                }
                InfoPanelUpdate();
            }
            // If we hit nothing:
            else {
                ResetAll();
            }
        }

        // Right click:
        if (Input.GetMouseButton(1)) {
            if(hexTileMap.tilesInMovementRange.Count > 0) {
                hexTileMap.UpdateTileDistances();
            }
        }
    }

    void InfoPanelUpdate() {
        // Tile has been selected:
        if (CurrentTile != null) {
            // Tile has been explored:
            if(CurrentTile.HasBeenExplored) {
                if (CurrentTile.UnitOnTop == null)
                    hud.UpdateInfoPanel(CurrentTile, null);
                else {
                    if (CurrentTile.UnitOnTop.IsVisible) {
                        hud.UpdateInfoPanel(CurrentTile, CurrentTile.UnitOnTop);
                    }
                    else hud.UpdateInfoPanel(CurrentTile, null);
                }
            }
            // Tile hasn't been explored:
            else {
                hud.UpdateInfoPanel(null, null);
            }
        }
        // Nothing to show:
        else hud.UpdateInfoPanel(null, null);
    }

    public void ResetSelection() {
        hexTileMap.ResetTileDistances();
        if (PreviousTile != null) {
            PreviousTile.IsSelected = false;
            PreviousTile = null;
        }
        if (CurrentTile != null) {
            CurrentTile.IsSelected = false;
            CurrentTile = null;
        }
        SelectedUnit = null;
    }

    // Reset selection, distances, and hide info panel:
    private void ResetAll() {
        ResetSelection();
        hexTileMap.ResetAttackRange();
        InfoPanelUpdate();
    }

    // Moves the unit from one tile to the other, currently done by teleportation
    // TODO: proper pathfinding (moving one tile at a time until reaching destination)
    private void MoveUnit(Tile hitTile) {
        // Reduce unit movement points:
        SelectedUnit.MovementCurrent -= hitTile.Distance;
        SelectedUnit.Tile = hitTile;

        // Unit y position:
        float yPos = (hitTile.Height * 0.2f) + 0.25f; // Placeholder unit model height settings
        // Place the unit higher if the unit is Flying type:
        if (SelectedUnit.UnitType.Contains("Flying")) yPos += 0.4f; 
        
        // Move selected unit to hit tile:
        SelectedUnit.transform.localPosition = new Vector3(hitTile.transform.position.x, yPos, hitTile.transform.position.z);

        // Remove unit from old tile:
        PreviousTile.UnitOnTop = null;

        // Set hit tile as current tile:
        CurrentTile = hitTile;
        CurrentTile.IsSelected = true;

        // Set unit on top for the new tile:
        CurrentTile.UnitOnTop = SelectedUnit;

        // Do visibility stuff if Fog of War or Darkness are enabled:
        if(hexTileMap.FogOfWarEnabled || hexTileMap.DarknessEnabled) {
            // Reduce visibility in previosly seen tiles:
            PreviousTile.DecreaseVisibilityInRange(CurrentTile.UnitOnTop);

            // Give a vision bonus to unit based on tile height:
            CurrentTile.UnitOnTop.Vision = CurrentTile.UnitOnTop.scriptableUnitCard.vision + (CurrentTile.Height - 1);

            // Increase visibility in seeable tiles:
            CurrentTile.IncreaseVisibilityInRange(CurrentTile.UnitOnTop);
        }

        // Show new distances:
        CurrentTile.ShowDistances();

        // Attack range:
        CurrentTile.ShowAttackRange();
    }
}
