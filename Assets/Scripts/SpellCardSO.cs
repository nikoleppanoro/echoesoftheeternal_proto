﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[CreateAssetMenu(fileName = "Data", menuName = "ScriptableObjects/Cards/SpellCard", order = 2)]

public class SpellCardSO: CardSO {
    [Header("Spell Card Info")]
    public List<Ability> spellAbilities;
}
