﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour
{
    public string playerName;
    private string playerID;
    public Deck playerDeck;
    public DiscardPile playerDiscardPile;
    public PlayerHand playerHand;
    public Resources playerResources;
    public Color playerColor;
    public Card cardInHand;

    public List<Unit> ownedUnits;
    public List<ControlPoint> ownedControlPoints;
    public List<GameObject> exploredTiles;

    public string PlayerID { 
        get => playerID; 
        set => playerID = value;
    }

    void Start() {
        playerDeck.player = this;
        playerDiscardPile.player = this;
        PlayerID = gameObject.name;
    }

    private void OnEnable() {
        TurnManager.OnTurnEnd += TurnEnd;
        TurnManager.OnTurnStart += TurnStart;
    }
    private void OnDisable() {
        TurnManager.OnTurnEnd -= TurnEnd;
        TurnManager.OnTurnStart -= TurnStart;
    }

    public void TurnStart() {
        // If there are no cards left in deck, the player loses:
        if(playerDeck.playerCards.Count == 0) {
            MessageManager.Show("Out of cards!");
            // TODO: stuff that happens when the player loses:
        }
        else {
            // If the player has less than 7 cards in hand, draw a card from deck:
            if (playerHand.cardsInHand.Count < playerHand.cardsInHandLimit) {
                playerDeck.DrawCard(1);
            }
            // Card limit reached:
            else {
                MessageManager.Show("Card limit reached, card was not drawn.");
            }
        }
    }

    public void TurnEnd() {
        this.gameObject.SetActive(false);
    }
}

