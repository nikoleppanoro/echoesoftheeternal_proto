﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Deck : MonoBehaviour
{
    public Player player;
    public List<CardSO> playerCards;
    public List<GameObject> cardsInDeck;
    public PlayerHand playerHand;
    public GameObject unitCardPrefab, spellCardPrefab;

    void Start() {
        // Instantiate deck:
        for (int i = 0; i < playerCards.Count; i++) {
            GameObject card = null;
            // Unit card:
            if (playerCards[i].GetType() == typeof(UnitCardSO)) {
                card = Instantiate(unitCardPrefab, this.transform);
                card.GetComponent<UnitCard>().card = (UnitCardSO) playerCards[i];
            }
            // Spell card:
            else if(playerCards[i].GetType() == typeof(SpellCardSO)) {
                card = Instantiate(spellCardPrefab, this.transform);
                card.GetComponent<SpellCard>().card = (SpellCardSO)playerCards[i];
            }
            // Owner:
            card.GetComponent<Card>().Owner = player;

            // Card name:
            card.gameObject.name = "Card_" + playerCards[i].name;
            // 
            card.GetComponent<Card>().enabled = false;
            cardsInDeck.Add(card);
        }
        ShuffleDeck();
        DrawCard(4);
    }
    public void AddCardToDeck(GameObject card, bool shuffleAfterwards) {
        cardsInDeck.Add(card);
        card.transform.SetParent(this.gameObject.transform);
        if (shuffleAfterwards) {
            ShuffleDeck();
        }
    }

    public void DrawCard(int drawCount) {
        for(int i = 0; i < drawCount; i++) {
            if (cardsInDeck.Count > 0) {
                // Add card to hand:
                playerHand.cardsInHand.Add(cardsInDeck[0]);

                // Remove card from deck:
                cardsInDeck[0].GetComponent<Card>().enabled = true;
                cardsInDeck[0].transform.SetParent(playerHand.transform);
                cardsInDeck.RemoveAt(0);
            }
            else {
                Debug.Log("Out of cards!");
            }
        }
        // Set card spacing:
        playerHand.setCardSpacing();
    }
    public void ShuffleDeck() {
        // Do some magical stuff to shuffle the cards: 
        for (int i = 0; i < cardsInDeck.Count; i++) {
            GameObject temp = cardsInDeck[i];
            int randomIndex = Random.Range(i, cardsInDeck.Count);
            cardsInDeck[i] = cardsInDeck[randomIndex];
            cardsInDeck[randomIndex] = temp;
        }
    }
}
