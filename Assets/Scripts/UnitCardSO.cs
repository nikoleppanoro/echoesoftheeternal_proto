﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[CreateAssetMenu(fileName = "Data", menuName = "ScriptableObjects/Cards/UnitCard", order = 1)]

public class UnitCardSO : CardSO {
    [Header("Unit Card Info")]
    public string attackType;
    public GameObject unitModel;
    public List<Ability> unitAbilities;

    [Header("Unit Stats")]
    public int health;
    public int attackPower;
    public int attackRangeMin, attackRangeMax;
    public int vision;
    public int movementPoints;
}
