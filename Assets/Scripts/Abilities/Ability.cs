﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Ability : ScriptableObject
{
    [Header("Ability Info")]
    public string abilityName;
    public string description;
    public enum TargetType {
        AnyUnit,
        AnyControlPoint,
        EnemyControlPoint,
        EnemyUnit,
        FriendlyControlPoint,
        FriendlyUnit,
        Tile
    };
    public TargetType targetType;

    [Header("Events")]
    public List<AbilityEvent> abilityEvents;

    public void TriggerAbility(AbilityEventArgs args) {
        foreach (AbilityEvent abilityEvent in abilityEvents) {
            abilityEvent.Trigger(args);
        }
    }
}