﻿using UnityEngine;
using System.Collections;

    public interface IAbilityUser
    {
        GameObject gameObject { get; }
        Transform transform { get; }
    }
