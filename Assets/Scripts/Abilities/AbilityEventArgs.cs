﻿using System.Collections;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using UnityEngine;

    public struct AbilityEventArgs
    {
        public Ability ability { get; set; }

        public IAbilityUser caster { get; set; }

        public GameObject source { get; set; }

        public GameObject target { get; set; }

        public AbilityEventArgs(Ability ability, IAbilityUser caster, GameObject target)
        {
            this.ability = ability;
            this.caster = caster;
            this.source = caster.gameObject;
            this.target = target;
        }
    }
