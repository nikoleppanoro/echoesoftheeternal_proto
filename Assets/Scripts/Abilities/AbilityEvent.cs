﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class AbilityEvent {
    public enum AbilitySpawnPosition {
        Target,  // Target of event
        Caster,  // Original caster
        Source   // Source of event
    }
    public AbilitySpawnPosition spawnPosition;

    public List<EffectSO> abilityEffects;

    public void Trigger(AbilityEventArgs args) {
        if(abilityEffects.Count > 0) {
            foreach (EffectSO effect in abilityEffects) {
                effect.Apply(args.target, args.source, args.caster);
            }
        }
    }
}
