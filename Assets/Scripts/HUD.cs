﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class HUD : MonoBehaviour
{
    public GameObject infoPanel;
    public MouseController mouseController;

    [Header("Tile Info")]
    public GameObject tileInfoPanel;
    public TextMeshProUGUI tileType;
    public TextMeshProUGUI tileHeight;
    public TextMeshProUGUI tileDescription;
    [Header("Unit Info")]
    public GameObject unitInfoPanel;
    public TextMeshProUGUI unitName;
    public TextMeshProUGUI unitType;
    public TextMeshProUGUI unitHealth;
    public TextMeshProUGUI unitAttack;
    public TextMeshProUGUI unitRange;
    public TextMeshProUGUI unitVision;
    public TextMeshProUGUI unitMovement;

    void Start() {
        UpdateInfoPanel(null, null);
    }

    public void UpdateInfoPanel(Tile currentTile, Unit unitOnTop) {
        if (currentTile != null) {
            infoPanel.SetActive(true);
            // Update tile info:
            tileType.text = currentTile.ScriptableTile.tileName;
            tileHeight.text = "Height: " + currentTile.Height.ToString();
            tileDescription.text = currentTile.ScriptableTile.description;

            if (unitOnTop != null) {
                unitInfoPanel.SetActive(true);
                // Update unit info:
                Unit unit = unitOnTop.GetComponent<Unit>();
                unitName.text = unit.scriptableUnitCard.cardName;
                unitType.text = unit.scriptableUnitCard.cardType;
                unitHealth.text = "Health: " + unit.HealthCurrent.ToString() + "/" +  unit.HealthMax.ToString();
                unitAttack.text = "Attack: " +  unit.AttackPower.ToString();
                unitRange.text = "Attack range: " + unit.AttackRangeMin.ToString() + "-" + unit.AttackRangeMax.ToString();
                unitVision.text = "Vision: " + unit.Vision.ToString();
                unitMovement.text = "Movement: " + unit.MovementCurrent.ToString() + "/" + unit.MovementMax.ToString();
            }
            else {
                unitInfoPanel.SetActive(false);
            }
        }
        else {
            infoPanel.SetActive(false);
            unitInfoPanel.SetActive(false);
            mouseController.ResetSelection();
        }
    }
    public void ExitGame() {
        Application.Quit();
    }

    public void RestartGame() {
        TurnManager.PlayerIndex = 0;
        UnityEngine.SceneManagement.SceneManager.LoadScene(UnityEngine.SceneManagement.SceneManager.GetActiveScene().name);
    }
}
