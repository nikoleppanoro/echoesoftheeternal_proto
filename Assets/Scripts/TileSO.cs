﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Data", menuName = "ScriptableObjects/Tile", order = 1)]

public class TileSO : ScriptableObject
{
    [Header("Info")]
    public GameObject tile;
    public GameObject objectOnTop;
    public string tileName;
    public string description;
    public float movementCost;
    public bool blocksLandMovement;
    public bool blocksSeaMovement;
    public bool blocksLineOfSight;
}