﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DiscardPile : MonoBehaviour {
    public Player player;
    public List<GameObject> cardsInPile;
    public PlayerHand playerHand;

    public void AddCardToDiscardPile(GameObject card) {
        card.transform.SetParent(this.transform);
        cardsInPile.Add(card);
        // Disable card:
        card.GetComponent<Card>().enabled = false;
    }

    public void RemoveCardFromDiscardPile(GameObject card) {
        card.transform.SetParent(card.GetComponent<Card>().Owner.playerHand.transform);
        cardsInPile.Remove(card);
        // Enable card:
        card.GetComponent<Card>().enabled = true;
    }
}
