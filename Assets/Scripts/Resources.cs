﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
public class Resources : MonoBehaviour
{
    public Image resImage, specialResImage;
    public GameObject standardResPanel, specialResPanel;
    public TextMeshProUGUI standardResText, specialResText;
    [SerializeField] int standardResMax = 10, standardResCurrent = 0, specialResMax = 10, specialResCurrent = 0;
    [SerializeField] List<Image> standardResList, specialResList;

    public int StandardResMax { get => standardResMax; set => standardResMax = value; }
    public int StandardResCurrent { get => standardResCurrent; set => standardResCurrent = value; }
    public int SpecialResMax { get => specialResMax; set => specialResMax = value; }
    public int SpecialResCurrent { get => specialResCurrent; set => specialResCurrent = value; }

    void Start()
    {
        standardResList = new List<Image>();
        specialResList = new List<Image>();
    }

    public void AddMana(int amount) {
        for(int i = 0; i < amount; i++) {
            if (StandardResCurrent < StandardResMax) {
                StandardResCurrent++;
                Image img = Instantiate(resImage, standardResPanel.transform);
                standardResList.Add(img);
                standardResText.text = "Standard Resources: " + StandardResCurrent + "/" + StandardResMax;
            }
            else return;
        }
    }

    public void AddSpecialMana(int amount) {
        for (int i = 0; i < amount; i++) {
            if (SpecialResCurrent < SpecialResMax) {
                SpecialResCurrent++;
                Image img = Instantiate(specialResImage, specialResPanel.transform);
                specialResList.Add(img);
                specialResText.text = "Special Resources: " + SpecialResCurrent + "/" + SpecialResMax;
            }
            else return;
        }
    }

    public void ReduceMana(int amount) {
        for (int i = 0; i < amount; i++) {
            if (StandardResCurrent > 0) {
                StandardResCurrent--;
                Object.Destroy(standardResList[0].gameObject);
                standardResList.Remove(standardResList[0]);
                standardResText.text = "Standard Resources: " + StandardResCurrent + "/" + StandardResMax;
            }
            else return;
        }
    }

    public void ReduceSpecialMana(int amount) {
        for (int i = 0; i < amount; i++) {
            if (SpecialResCurrent > 0) {
                SpecialResCurrent--;
                Object.Destroy(specialResList[0].gameObject);
                specialResList.Remove(specialResList[0]);
                specialResText.text = "Special Resources: " + SpecialResCurrent + "/" + SpecialResMax;
            }
            else return;
        }
    }
}
