﻿using Cinemachine;
using UnityEngine;

public class CameraController : MonoBehaviour {

    public GameObject cameraTarget;
    public CinemachineVirtualCamera vCam;
    public HexTileMap hexTileMap;

    [Header("Rotation")]
    [SerializeField] private Vector2 mouseLastPosition, mouseCurrentPosition;
    public float maxTurnAngle = 120f;

    [Header("Zoom")]
    public float cameraDistance = 10f;
    public float cameraMinDistance = 3f;
    public float cameraMaxDistance = 12f;
    private Vector2 scrollDelta;

    [Header("Movement")]
    public bool mouseMovementEnabled;
    public float movementSpeed;
   
    Vector3 horMovement, verMovement;
    Vector3 moveDirection;
    Vector3 newPosition;
    Vector3 startPosition;
    Quaternion startRotation;


    private void Start() {
        vCam.GetComponent<CinemachineFollowZoom>().m_Width = cameraDistance;
        transform.LookAt(cameraTarget.transform);
        startPosition = transform.position;
        startRotation = transform.rotation;
        newPosition = transform.position; 
    }

    private void Update() {
        CameraRotation();
        CameraZoom();
        CameraMovement();

        if(Input.GetKeyDown(KeyCode.R)) {
            ResetCamera();
        }
    }

    private void CameraRotation() {
        if (Input.GetMouseButtonDown(1)) {
            mouseLastPosition = Input.mousePosition;
        }
        if (Input.GetMouseButton(1)) {
            mouseCurrentPosition = Input.mousePosition;
            Vector2 vector = new Vector2(mouseLastPosition.x - mouseCurrentPosition.x, mouseLastPosition.y - mouseCurrentPosition.y);

            float angle = Mathf.Clamp(vector.x, -maxTurnAngle, maxTurnAngle);
            gameObject.transform.RotateAround(cameraTarget.transform.position, Vector3.up, angle * Time.deltaTime);
        }
    }
    private void CameraZoom() {
        if (Input.mouseScrollDelta != Vector2.zero) {
            // Scroll up: (0.0, 1.0), scroll down: (0.0, -1.0)
            scrollDelta = Input.mouseScrollDelta;

            cameraDistance -= scrollDelta.y;
            cameraDistance = Mathf.Clamp(cameraDistance, cameraMinDistance, cameraMaxDistance);

            vCam.GetComponent<CinemachineFollowZoom>().m_Width = cameraDistance;
        }
    }
    private void CameraMovement() {
        // Mouse movement:
        if (mouseMovementEnabled) {
            Vector3 pos = Input.mousePosition;
            // Near left edge:
            if (pos.x < 50) {
                horMovement = -transform.right * movementSpeed;
            }
            // Near right edge:
            else if (pos.x > Camera.main.pixelWidth - 50) {
                horMovement = transform.right * movementSpeed;
            }
            else {
                horMovement = Vector3.zero;
            }
            // Near bottom edge: 
            if (pos.y < 50) {
                verMovement = -transform.forward * movementSpeed;
            }
            // Near top edge:
            else if (pos.y > Camera.main.pixelHeight - 50) {
                verMovement = transform.forward * movementSpeed;
            }
            else {
                verMovement = Vector3.zero;
            }
            moveDirection = verMovement + horMovement;
            newPosition = transform.position + moveDirection;
        }

        // WASD / Arrow keys:
        if (Input.GetAxis("Vertical") != 0 || Input.GetAxis("Horizontal") != 0) {
            verMovement = transform.forward * Input.GetAxisRaw("Vertical") * movementSpeed;
            horMovement = transform.right * Input.GetAxisRaw("Horizontal") * movementSpeed;
            moveDirection = verMovement + horMovement;
            newPosition = transform.position + moveDirection;
        }

        if (transform.position != newPosition) {
            transform.position = Vector3.Lerp(transform.position, newPosition, Time.deltaTime);
            if (hexTileMap.tilesInMovementRange.Count > 0) {
                hexTileMap.UpdateTileDistances();
            }
        }
    }

    // Resets camera position to original position:
    void ResetCamera() {
        transform.position = moveDirection = startPosition;
        transform.rotation = startRotation;
        newPosition = transform.position;
        horMovement = verMovement = Vector3.zero;
    }

    public void MoveCameraTo(Vector3 position) {
        cameraTarget.transform.position = newPosition = position;
        newPosition = position;
    }
}

