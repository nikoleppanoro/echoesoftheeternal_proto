﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class TurnManager : MonoBehaviour {

    [Header("Other")]
    [SerializeField] bool gameIsRunning = false;
    public HexTileMap hexTileMap;

    [Header("Players")]
    [SerializeField] List<Player> players;
    Player currentPlayer;
    int currentPlayerIndex = 0;
    public static int PlayerIndex = 0;

    [Header("Turns")]
    [SerializeField] int turnCounter = 1;
    [SerializeField] float turnTimeLimitMax = 100;
    [SerializeField] float turnTimeLimitCurrent = 100;

    [Header("In-game")]
    public Canvas inGameCanvas;
    public TextMeshProUGUI currentPlayerName, inGameTurnNumber, turnTimer;

    [Header("Turn Start")]
    public Canvas turnStartCanvas;
    public TextMeshProUGUI startTurnNumber, startPlayerText;

    [Header("Messages")]
    public Canvas messageCanvas;
    WaitForSeconds waitDelay = new WaitForSeconds(2);

    [Header("Map Editing")]
    public Canvas mapEditCanvas;

    // Events:
    public delegate void TurnStart();
    public delegate void TurnEnd();
    public static event TurnStart OnTurnStart;
    public static event TurnEnd OnTurnEnd;

    public Player CurrentPlayer { get => currentPlayer; set => currentPlayer = value; }
    public int TurnCounter { get => turnCounter; set => turnCounter = value; }
    public List<Player> Players { get => players; set => players = value; }

    private void Start() {
        if(!hexTileMap.startInEditMode) {
            mapEditCanvas.gameObject.SetActive(false);

            turnTimeLimitCurrent = turnTimeLimitMax;
            CurrentPlayer = Players[0];

            foreach (Player player in Players) {
                player.gameObject.SetActive(false);
            }

            StartCoroutine(TurnStartCoroutine(CurrentPlayer));
        }
        else {
            // Turn off some stuff:
            // Players:
            foreach (Player player in Players) {
                player.gameObject.SetActive(false);
            }
            // Fog of war:
            hexTileMap.FogOfWarEnabled = false;
            hexTileMap.DarknessEnabled = false;
            // HUDs:
            inGameCanvas.gameObject.SetActive(false);
            turnStartCanvas.gameObject.SetActive(false);
        }
    }

    private void Update() {
        //Turn timer:
        if(gameIsRunning) {
            turnTimeLimitCurrent -= Time.deltaTime;
            turnTimer.text = "Time left: " + turnTimeLimitCurrent.ToString("F1") + " s";
            if (turnTimeLimitCurrent < 0) {
                EndTurnClick();
            }
        }
    }

    public void EndTurnClick() {
        // Fixes a bug with the player holding a card and the turn ends:
        if (CurrentPlayer.cardInHand != null) {
            CurrentPlayer.cardInHand.ReturnCardToHand();
        }

        if(OnTurnEnd != null) {
            //Trigger TurnEnd events:
            OnTurnEnd();

            // Reset turn timer:
            turnTimeLimitCurrent = turnTimeLimitMax;

            // Stop game from running:
            gameIsRunning = false;

            // Switch players:
            ++currentPlayerIndex;
            if(currentPlayerIndex == Players.Count) {
                currentPlayerIndex = 0;
                TurnCounter++;
            }
            CurrentPlayer = Players[currentPlayerIndex];

            StartCoroutine(TurnStartCoroutine(CurrentPlayer));
        }
    }

    IEnumerator TurnStartCoroutine(Player player) {
        // Display turn info:
        inGameCanvas.gameObject.SetActive(false);
        messageCanvas.gameObject.SetActive(false);
        turnStartCanvas.gameObject.SetActive(true);
        
        //Clear any shown messages:
        MessageManager.ClearMessages();

        // Change text:
        currentPlayerName.text = CurrentPlayer.playerName;
        currentPlayerName.color = CurrentPlayer.playerColor;
        inGameTurnNumber.text = startTurnNumber.text = "Turn " + TurnCounter;
        startPlayerText.text = player.playerName + " Start!";
        
        // Wait for 2 seconds:
        yield return waitDelay;

        // Hide turn info:
        inGameCanvas.gameObject.SetActive(true);
        turnStartCanvas.gameObject.SetActive(false);

        // Activate current player:
        gameIsRunning = true;
        messageCanvas.gameObject.SetActive(true);
        player.gameObject.SetActive(true);

        //Trigger TurnStart events:
        OnTurnStart();
    }
}
