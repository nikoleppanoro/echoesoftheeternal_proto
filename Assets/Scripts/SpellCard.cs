﻿using UnityEngine;
using UnityEngine.EventSystems;
using TMPro;

public class SpellCard : Card, IAbilityUser {
    private SpellCardSO spellCard;

    private new void Start() {
        base.Start();
        spellCard = (SpellCardSO) card;

        // Stats:
        manaCostText.text = spellCard.manaCost.ToString() + "/" + spellCard.specialManaCost.ToString();

        // Other text:
        cardName.text = spellCard.cardName;
        infoText.text = spellCard.cardType + ", " + spellCard.cardRarity.ToString();
        flavorText.text = spellCard.flavorText;
        abilityText.text = spellCard.abilityText;

        // Image: 
        artworkImage = spellCard.cardArtworkImage;
    }

    // Checks for tile visibility and target type for a spell card ability
    public override bool CheckIfValidTarget(GameObject target) {
        bool targetIsValid = true;

        // Is the target tile visible:
        if(!target.GetComponent<Tile>().IsVisible) {
            targetIsValid = false;
            MessageManager.Show("Spells must be cast on a visible tile, unit or control point!");
            return targetIsValid;
        }
        // Is the target type correct:
        foreach (Ability ability in spellCard.spellAbilities) {
            switch(ability.targetType) {
                case Ability.TargetType.Tile:
                    if (target.GetComponent<Tile>() == null) {
                        targetIsValid = false;
                        MessageManager.Show("This spell card requires a tile as it's target!");
                        break;
                    }
                    break;
                case Ability.TargetType.AnyControlPoint:
                case Ability.TargetType.EnemyControlPoint:
                case Ability.TargetType.FriendlyControlPoint:
                    if (target.GetComponent<ControlPoint>() == null) {
                        targetIsValid = false;
                        MessageManager.Show("This spell card requires a control point as it's target!");
                        break;
                    }
                    break;
                case Ability.TargetType.AnyUnit:
                case Ability.TargetType.EnemyUnit:
                case Ability.TargetType.FriendlyUnit:
                    if (target.GetComponent<Tile>().UnitOnTop == null) {
                        targetIsValid = false;
                        MessageManager.Show("This spell card requires a unit as it's target!");
                        break;
                    }
                    break;
            }
        }
        return targetIsValid;
    }

    public override void CastSpell(GameObject target) {
        MessageManager.Show("You have cast the spell '" + card.cardName + "'!");
        // Trigger each spell card ability:
        foreach (Ability ability in spellCard.spellAbilities) {
            AbilityEventArgs args = new AbilityEventArgs(ability, this, target);
            ability.TriggerAbility(args);
        }
    }
}