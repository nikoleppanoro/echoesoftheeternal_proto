﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Message : MonoBehaviour {
    [SerializeField] float timer = 5f;
    public MessageManager messageManager;

    private void Start() {
        messageManager = GameObject.Find("MessagePanel").GetComponent<MessageManager>();
        // Set the timer based on the number of messages being displayed;
        timer = 5f + messageManager.displayedMessages.Count; 
    }

    void Update()
    {
        timer -= Time.deltaTime;
        if (timer <= 0) RemoveMessage();
    }

    void RemoveMessage() {
        MessageManager.Remove(gameObject);
        Object.Destroy(gameObject);
    }
}
