﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerHand : MonoBehaviour {
    public List<GameObject> cardsInHand;
    public int cardsInHandLimit = 7;

    // Card spacing stuff:
    float cardWidth = 175f;
    float panelWidth;

    private void Start() {
        cardsInHand = new List<GameObject>();

        foreach (Transform child in transform) {
            cardsInHand.Add(child.gameObject);
        }

        panelWidth = gameObject.GetComponent<RectTransform>().rect.width;

        setCardSpacing();
    }

    // Sets the spacing of the cards in the players hand
    public void setCardSpacing() {
        int numberOfCards = cardsInHand.Count;
        HorizontalLayoutGroup layoutGroup = gameObject.GetComponent<HorizontalLayoutGroup>();

        layoutGroup.spacing = (panelWidth - (cardWidth * cardsInHand.Count)) / (cardsInHand.Count - 1);

        if(layoutGroup.spacing > 0) {
            layoutGroup.spacing = 0;
        }
    }
}