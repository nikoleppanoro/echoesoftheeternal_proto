﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Unit : MonoBehaviour, IAbilityUser
{
    public UnitCard unitCard;
    public UnitCardSO scriptableUnitCard;

    [Header("Other")]
    [SerializeField] Player owner;
    [SerializeField] Tile tile;
    HexTileMap hexTileMap;
    TurnManager turnManager;

    [Header("Stats")]
    [SerializeField] int healthMax;
    [SerializeField] int healthCurrent;
    [SerializeField] int attackPower;
    [SerializeField] int attackRangeMin, attackRangeMax;
    [SerializeField] string attackType;
    [SerializeField] int vision;
    [SerializeField] float movementMax, movementCurrent;
    [SerializeField] string unitType;

    [SerializeField] bool isVisible = true;
    [SerializeField] bool canAct = true;

    [Header("Effects")]
    public List<Effect> effects = new List<Effect>();

    public Player Owner { get => owner; set => owner = value; }
    public Tile Tile { get => tile; set => tile = value; }
    public bool IsVisible { 
        get => isVisible;
        set {
            isVisible = value;
            if(isVisible) {
                GetComponent<MeshRenderer>().enabled = true;
            }
            else {
                GetComponent<MeshRenderer>().enabled = false;
            }
        }
    }
    public bool CanAct { get => canAct; set => canAct = value; }
    public int HealthMax { get => healthMax; set => healthMax = value; }
    public int HealthCurrent { get => healthCurrent; set => healthCurrent = value; }
    public int AttackPower { get => attackPower; set => attackPower = value; }
    public int AttackRangeMin { get => attackRangeMin; set => attackRangeMin = value; }
    public int AttackRangeMax { get => attackRangeMax; set => attackRangeMax = value; }
    public string AttackType { get => attackType; set => attackType = value; }
    public int Vision { get => vision; set => vision = value; }
    public float MovementMax { get => movementMax; set => movementMax = value; }
    public float MovementCurrent { get => movementCurrent; set => movementCurrent = value; }
    public string UnitType { get => unitType; set => unitType = value; }

    void Start() {
        SetupUnitStats();

        hexTileMap = Tile.hexTileMap;
        turnManager = GameObject.Find("TurnManager").GetComponent<TurnManager>();

        // Increase visibility:
        Tile.IncreaseVisibilityInRange(this);

        MessageManager.Show("You have summoned the " + scriptableUnitCard.cardName + "!");
    }

    void SetupUnitStats() {
        // Copy values from scriptable object card:
        HealthCurrent = HealthMax = scriptableUnitCard.health;
        AttackPower = scriptableUnitCard.attackPower;
        AttackRangeMin = scriptableUnitCard.attackRangeMin;
        AttackRangeMax = scriptableUnitCard.attackRangeMax;
        AttackType = scriptableUnitCard.attackType;
        Vision = scriptableUnitCard.vision;
        MovementCurrent = MovementMax = scriptableUnitCard.movementPoints;
        UnitType = scriptableUnitCard.cardType;
    }

    private void OnEnable() {
        TurnManager.OnTurnStart += TurnStart;
        TurnManager.OnTurnEnd += TurnEnd;
    }

    private void OnDisable() {
        TurnManager.OnTurnStart -= TurnStart;
        TurnManager.OnTurnEnd -= TurnEnd;
    }

    private void OnDestroy() {
        // TODO: if unit dies on owner's turn, decrease vision
        if(turnManager.CurrentPlayer == Owner) {
            Tile.DecreaseVisibilityInRange(this);
        }

        Tile.UnitOnTop = null;
        Owner.ownedUnits.Remove(this);
    }

    void TurnStart() {
        if(turnManager.CurrentPlayer == Owner) {
            // Reset movement:
            RefreshMovement();
            // Apply TurnStart effects:
            ApplyTurnStartEffects();
            // Increase visibility:
            Tile.IncreaseVisibilityInRange(this);
        }
    }

    void TurnEnd() {
        if (turnManager.CurrentPlayer == Owner) {
            // Apply TurnEnd effects:
            ApplyTurnEndEffects();
            // Decrease visibility:
            Tile.DecreaseVisibilityInRange(this);
        }
    }


    public void Attack(Unit target) {
        MessageManager.Show(Owner.playerName + "'s " + scriptableUnitCard.cardName + " is attacking " + target.Owner.playerName + "'s " + target.scriptableUnitCard.cardName + "!");
        target.TakeDamage(AttackPower);
        MovementCurrent = 0;
        CanAct = false;
    }

    public void TakeDamage(int amount) {
        HealthCurrent -= amount;
        MessageManager.Show(Owner.playerName + "'s " + gameObject.name + " took " + amount + " damage! (Health: " + HealthCurrent + "/" + HealthMax + ")");
        if (HealthCurrent <= 0) {
            MessageManager.Show(Owner.playerName + "'s " + gameObject.name + " was destroyed!");
            Destroy(gameObject);
        }
    }

    public void Heal(int amount) {
        HealthCurrent += amount;
        if (HealthCurrent > HealthMax) {
            HealthCurrent = HealthMax;
        }
        MessageManager.Show(Owner.playerName + "'s " + gameObject.name + " healed for " + amount + " damage! (Health: " + HealthCurrent + "/" + HealthMax + ")");
    }
    void ApplyTurnStartEffects() {
        // Apply effects that activate at start of turn:
        if (effects.Count > 0) {
            for (int i = 0; i < effects.Count; i++) {
                switch (effects[i].effectSO.effectActivationType) {
                    case EffectSO.EffectActivationType.AnyTurnStart:
                    case EffectSO.EffectActivationType.AnyLastTurnStart:
                        effects[i].Activate(gameObject, gameObject, this);
                        break;
                    case EffectSO.EffectActivationType.CasterTurnStart:
                    case EffectSO.EffectActivationType.CasterLastTurnStart:
                        if (turnManager.CurrentPlayer == effects[i].Caster)
                            effects[i].Activate(gameObject, gameObject, this);
                        break;
                    case EffectSO.EffectActivationType.EnemyTurnStart:
                    case EffectSO.EffectActivationType.EnemyLastTurnStart:
                        if (turnManager.CurrentPlayer != effects[i].Caster)
                            effects[i].Activate(gameObject, gameObject, this);
                        break;
                    case EffectSO.EffectActivationType.OwnerTurnStart:
                    case EffectSO.EffectActivationType.OwnerLastTurnStart:
                        if (effects[i].Owner == turnManager.CurrentPlayer)
                            effects[i].Activate(gameObject, gameObject, this);
                        break;
                    default:
                        break;
                }
            }
        }
    }
    void ApplyTurnEndEffects() {
        // Apply effects that activate at the end of turn:
        if (effects.Count > 0) {
            for (int i = 0; i < effects.Count; i++) {
                switch (effects[i].effectSO.effectActivationType) {
                    case EffectSO.EffectActivationType.AnyTurnEnd:
                    case EffectSO.EffectActivationType.AnyLastTurnEnd:
                        effects[i].Activate(gameObject, gameObject, this);
                        break;
                    case EffectSO.EffectActivationType.CasterTurnEnd:
                    case EffectSO.EffectActivationType.CasterLastTurnEnd:
                        if (turnManager.CurrentPlayer == effects[i].Caster)
                            effects[i].Activate(gameObject, gameObject, this);
                        break;
                    case EffectSO.EffectActivationType.EnemyTurnEnd:
                    case EffectSO.EffectActivationType.EnemyLastTurnEnd:
                        if (turnManager.CurrentPlayer != effects[i].Caster)
                            effects[i].Activate(gameObject, gameObject, this);
                        break;
                    case EffectSO.EffectActivationType.OwnerTurnEnd:
                    case EffectSO.EffectActivationType.OwnerLastTurnEnd:
                        if (effects[i].Owner == turnManager.CurrentPlayer)
                            effects[i].Activate(gameObject, gameObject, this);
                        break;
                    default:
                        break;
                }
            }
        }
    }
    public void RefreshMovement() {
        MovementCurrent = MovementMax;
        CanAct = true;
    }

    public void TriggerAbility(Ability ability, GameObject target) {
        AbilityEventArgs args = new AbilityEventArgs(ability, this, target);
        ability.TriggerAbility(args);
    }

    public Ability GetAbilityByName(string name) {
        foreach(Ability ability in scriptableUnitCard.unitAbilities) {
            if(ability.abilityName == name) {
                return ability;
            }
        }
        return null;
    }
}