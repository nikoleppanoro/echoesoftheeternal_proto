﻿using UnityEngine;
using UnityEngine.EventSystems;
using TMPro;

public class UnitCard : Card {
    UnitCardSO unitCard;
    public GameObject unitModel;

    [Header("Stats")]
    public TextMeshProUGUI health;
    public TextMeshProUGUI attackPower;
    public TextMeshProUGUI attackRange;
    public TextMeshProUGUI vision;
    public TextMeshProUGUI movement;

    bool valuesHaveBeenSet;

    new void Start()
    {
        base.Start();
        SetupCardValues();
    }

    public void SetupCardValues() {
        unitCard = (UnitCardSO)card;
        unitModel = unitCard.unitModel;

        // Stats:
        health.text = unitCard.health.ToString();
        attackPower.text = unitCard.attackPower.ToString();
        attackRange.text = unitCard.attackRangeMin.ToString()
            + "-" + unitCard.attackRangeMax.ToString();
        vision.text = unitCard.vision.ToString();
        movement.text = unitCard.movementPoints.ToString();
        manaCostText.text = unitCard.manaCost.ToString()
            + "/" + unitCard.specialManaCost.ToString();

        // Other text:
        cardName.text = unitCard.cardName;
        infoText.text = unitCard.cardType + ", " + unitCard.attackType + ", " + unitCard.cardRarity;
        flavorText.text = unitCard.flavorText;
        abilityText.text = unitCard.abilityText;

        // Card image:
        artworkImage = unitCard.cardArtworkImage;

        // 3d-model:
        unitModel = unitCard.unitModel;

        valuesHaveBeenSet = true;
    }

    public override void SpawnUnit(GameObject obj) {
        Debug.Log("Spawning a unit...");

        if (!valuesHaveBeenSet) SetupCardValues();

        // Unit y position:
        float yPos = (obj.GetComponent<Tile>().Height * 0.2f) + 0.25f; // Placeholder unit model height settings
        // Place the unit higher if the unit is Flying type:
        if (unitCard.cardType.Contains("Flying")) yPos += 0.4f;

        // Instantiate unit on tile:
        GameObject spawnedUnit = Instantiate(unitModel, new Vector3(obj.transform.position.x, yPos, obj.transform.position.z), Quaternion.identity);

        // Set unit values:
        spawnedUnit.GetComponent<Unit>().Owner = Owner;
        spawnedUnit.GetComponent<Unit>().Tile = obj.GetComponent<Tile>();
        spawnedUnit.GetComponent<Unit>().unitCard = this;
        spawnedUnit.GetComponent<Unit>().scriptableUnitCard = unitCard;
        spawnedUnit.transform.SetParent(GameObject.Find("Units").transform);
        spawnedUnit.name = cardName.text;
        spawnedUnit.GetComponent<MeshRenderer>().material.color = Owner.playerColor;
        Owner.ownedUnits.Add(spawnedUnit.GetComponent<Unit>());

        // Set tile values:
        obj.GetComponent<Tile>().UnitOnTop = spawnedUnit.GetComponent<Unit>();
    }
}