﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "New Effect", menuName = "ScriptableObjects/Effects/IncreaseVisibility")]
public class IncreaseVisibility : EffectSO {
    [Header("IncreaseVisibility Variables")]
    public Player player;                       // Which player's vision should be increased?
    public List<TileSO> targetTileTypes;        // What types of tiles that should be targeted?
    public int tileSearchRange;                 // How far should we search for tiles starting from the center?
    public int visionRange;                     // Thr range of vision for selected tiles

    public override void Apply(GameObject target, GameObject causer, object instigator) {
        Tile targetTile = target.gameObject.GetComponent<Tile>();
        List<Tile> tiles = new List<Tile>();
        // If a target tile type has been specified:
        if (targetTileTypes.Count > 0) {
            foreach (TileSO tileType in targetTileTypes) {
                // Get tiles in range that are of the specified type:
                tiles.AddRange(targetTile.GetTilesInRange(tileSearchRange, tileType));
            }
        }
        else {
            tiles = targetTile.GetTilesInRange(tileSearchRange, null);
        }

        // Increase visibility for those tiles:
        foreach (Tile tile in tiles) {
            tile.IncreaseVisibilityInRange(visionRange);
        }
    }
}
