﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "New Effect", menuName = "ScriptableObjects/Effects/DecreaseResourceGains")]
public class DecreaseResourceGains : EffectSO {
    [Header("DecreaseResourceGains Variables")]
    public int standardResourceDecrease;
    public int specialResourceDecrease;

    public override void Apply(GameObject target, GameObject causer, object instigator) {
        // Target control point:
        ControlPoint cp = target.GetComponent<ControlPoint>();

        // Decrease resource gains:
        cp.StandardResPerTurn -= standardResourceDecrease;
        cp.SpecialResPerTurn -= specialResourceDecrease;

        // Set gains to 0 if negative:
        if (cp.StandardResPerTurn < 0) {
            cp.StandardResPerTurn = 0;
        }
        if (cp.SpecialResPerTurn < 0) {
            cp.SpecialResPerTurn = 0;
        }
    }
}
