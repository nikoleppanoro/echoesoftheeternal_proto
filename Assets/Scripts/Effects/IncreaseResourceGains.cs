﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "New Effect", menuName = "ScriptableObjects/Effects/IncreaseResourceGains")]

public class IncreaseResourceGains : EffectSO {
    [Header("IncreaseResourceGains Variables")]
    public int standardResourceIncrease;
    public int specialResourceIncrease;

    public override void Apply(GameObject target, GameObject causer, object instigator) {
        // Target control point:
        ControlPoint cp = target.GetComponent<ControlPoint>();

        // Increase resource gains:
        cp.StandardResPerTurn += standardResourceIncrease;
        cp.SpecialResPerTurn += specialResourceIncrease;
    }
}
