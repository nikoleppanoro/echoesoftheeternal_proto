﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "New Effect", menuName = "ScriptableObjects/Effects/AlterResourceGains")]
public class AlterResourceGains : EffectSO {
    [Header("AlterResourceGains Variables")]
    public int standardResourceAmount;
    public int specialResourceAmount;

    public override void Apply(GameObject target, GameObject causer, object instigator) {
        // Target control point:
        ControlPoint cp = target.GetComponent<ControlPoint>();

        if(effectName == "Manna Rain") {
            standardResourceAmount = 0;
            specialResourceAmount = cp.StandardResPerTurn;
        }
        else if (effectName == "Manna Rain 2") {
            standardResourceAmount = 0;
            specialResourceAmount = -cp.StandardResPerTurn;
        }
        // Alter resource gains:
        cp.StandardResPerTurn += standardResourceAmount;
        cp.SpecialResPerTurn += specialResourceAmount;

        Debug.Log(standardResourceAmount + " to St.Res per turn, " + specialResourceAmount + " to Sp.Res per turn.");
        Debug.Log("New resource gains set! Standard: " + cp.StandardResPerTurn + ", Special: " + cp.SpecialResPerTurn);
    }
}
