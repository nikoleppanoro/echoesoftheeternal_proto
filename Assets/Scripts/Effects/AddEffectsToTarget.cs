﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "New Effect", menuName = "ScriptableObjects/Effects/AddEffectsToTarget")]
public class AddEffectsToTarget : EffectSO {
    [Header("AddEffectsToTarget Variables")]
    public List<EffectSO> effectsToBeAdded;
    Player player;
    Effect effect;
    TurnManager turnManager;

    public override void Apply(GameObject target, GameObject causer, object instigator) {    
        turnManager = GameObject.Find("TurnManager").GetComponent<TurnManager>();

        // When used from a spell card:
        if(causer.GetComponent<SpellCard>()!= null) {
            player = causer.GetComponent<SpellCard>().Owner;
        }
        // When used by a unit:
        else if (causer.GetComponent<Unit>() != null) {
            player = causer.GetComponent<Unit>().Owner;
        }
        // Loop for adding effects:
        for(int i = 0; i < effectsToBeAdded.Count; i++) {
            switch (effectsToBeAdded[i].effectTargetType) {
                // Targets a unit:
                case EffectSO.EffectTargetType.AnyUnit:
                case EffectSO.EffectTargetType.EnemyUnit:
                case EffectSO.EffectTargetType.FriendlyUnit:
                    Unit targetUnit = target.GetComponent<Tile>().UnitOnTop;
                    effect = targetUnit.gameObject.AddComponent<Effect>();
                    SetEffectValues(effect, i);
                    targetUnit.effects.Add(effect);
                    break;
                // Targets a control point:
                case EffectSO.EffectTargetType.AnyControlPoint:
                case EffectSO.EffectTargetType.EnemyControlPoint:
                case EffectSO.EffectTargetType.FriendlyControlPoint:
                    ControlPoint targetCP = target.GetComponent<ControlPoint>();
                    effect = targetCP.gameObject.AddComponent<Effect>();
                    SetEffectValues(effect, i);
                    targetCP.effects.Add(effect);
                    break;
                // Targets a tile:
                case EffectSO.EffectTargetType.Tile:
                    Tile targetTile = target.GetComponent<Tile>();
                    effect = targetTile.gameObject.AddComponent<Effect>();
                    SetEffectValues(effect, i);
                    targetTile.effects.Add(effect);
                    break;
            }

            // Activate effect if it should activate upon casting:
            if (effect.effectSO.effectActivationType == EffectSO.EffectActivationType.UponCasting
                || effect.effectSO.activatesImmediatelyWhenAdded) {
                effect.Activate(target, causer, instigator);
            }
        }
    }

    void SetEffectValues(Effect effect, int index) {
        effect.effectSO = effectsToBeAdded[index];
        effect.effectName = effect.effectSO.effectName;
        effect.Caster = this.player;

        effect.turnsInEffect = effect.effectSO.turnsInEffect;

        switch (effect.effectSO.effectActivationType) {
            case EffectActivationType.CasterTurnEnd:
            case EffectActivationType.CasterLastTurnEnd:
                if (effect.Caster == turnManager.CurrentPlayer)
                    effect.turnsInEffect++;
                break;
            case EffectActivationType.OwnerTurnEnd:
            case EffectActivationType.OwnerLastTurnEnd:
                if (effect.Owner == turnManager.CurrentPlayer)
                    effect.turnsInEffect++;
                break;
        }
    }
}
