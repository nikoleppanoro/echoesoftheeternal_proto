﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class EffectSO : ScriptableObject {
    [Header("Effect Info")]
    public string effectName;
    public int turnsInEffect = int.MaxValue;
    public bool activatesImmediatelyWhenAdded;
    public enum EffectActivationType {
        AnyTurnStart,
        AnyTurnEnd,
        AnyLastTurnStart,
        AnyLastTurnEnd,
        CasterTurnStart,
        CasterTurnEnd,
        CasterLastTurnStart,
        CasterLastTurnEnd,
        EnemyTurnStart,
        EnemyTurnEnd,
        EnemyLastTurnStart,
        EnemyLastTurnEnd,
        OwnerTurnStart,
        OwnerTurnEnd,
        OwnerLastTurnStart,
        OwnerLastTurnEnd,
        UponCasting,
    };
    public EffectActivationType effectActivationType;

    public enum EffectTargetType {
        AnyUnit,
        AnyControlPoint,
        EnemyControlPoint,
        EnemyUnit,
        FriendlyControlPoint,
        FriendlyUnit,
        Tile,
    };
    public EffectTargetType effectTargetType;

    public virtual void Apply(GameObject target, GameObject causer, object instigator) { }
}