﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "New Effect", menuName = "ScriptableObjects/Effects/ToggleResourceGains")]
public class ToggleResourceGains : EffectSO {
    [Header("ToggleResourceGains Variables")]
    public bool resourceGainsToggle;
    public override void Apply(GameObject target, GameObject causer, object instigator) {
        // Target control point:
        ControlPoint targetCP = target.GetComponent<ControlPoint>();

        targetCP.ResourceGainsAllowed = resourceGainsToggle;
    }
}
