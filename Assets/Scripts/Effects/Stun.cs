﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "New Effect", menuName = "ScriptableObjects/Effects/Stun")]
public class Stun : EffectSO {
    public override void Apply(GameObject target, GameObject causer, object instigator) {
        // Get the target unit:
        Unit targetUnit = null;
        if(target.GetComponent<Tile>() != null) {
            targetUnit = target.GetComponent<Tile>().UnitOnTop;
        }
        else if(target.GetComponent<Unit>() != null) {
            targetUnit = target.GetComponent<Unit>();
        }

        // Stop unit from acting and moving:
        targetUnit.MovementCurrent = 0;
        targetUnit.CanAct = false;

        MessageManager.Show(targetUnit.name + " is stunned! (cannot move or attack this turn)");
    }
}
