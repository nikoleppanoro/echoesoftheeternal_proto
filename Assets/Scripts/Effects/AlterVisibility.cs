﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "New Effect", menuName = "ScriptableObjects/Effects/AlterVisibility")]
public class AlterVisibility : EffectSO {
    [Header("AlterVisibility Variables")]
    public Player player;                       // Which player's vision should be increased?
    public List<TileSO> targetTileTypes;        // What types of tiles should be targeted?
    public int tileSearchRange;                 // How far should we search for tiles starting from the center?
    public int visionRange;                     // The range of vision for selected tiles. If set to negative, visibility will be decreased

    public override void Apply(GameObject target, GameObject causer, object instigator) {
        Tile targetTile = target.gameObject.GetComponent<Tile>();
        List<Tile> tiles = new List<Tile>();
        // If a target tile type has been specified:
        if (targetTileTypes.Count > 0) {
            foreach (TileSO tileType in targetTileTypes) {
                // Get tiles in range that are of the specified type:
                tiles.AddRange(targetTile.GetTilesInRange(tileSearchRange, tileType));
            }
        }
        else {
            tiles = targetTile.GetTilesInRange(tileSearchRange, null);
        }

        // Alter visibility for those tiles depending of the visionRange value:
        if(visionRange > 0) {
            foreach (Tile tile in tiles) {
                tile.IncreaseVisibilityInRange(visionRange);
            }
        }
        else if(visionRange < 0) {
            foreach (Tile tile in tiles) {
                tile.DecreaseVisibilityInRange(-visionRange);
            }
        }
        else if(visionRange == 0) {
            Debug.Log("visionRange was set to 0, visibility was not affected.");
        }
    }
}
