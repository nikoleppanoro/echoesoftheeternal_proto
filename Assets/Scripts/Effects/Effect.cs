﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Effect : MonoBehaviour
{
    public EffectSO effectSO;               // The scriptable object of the effect
    public string effectName;               // The name of the effect
    public int turnsInEffect;               // How many turns the effect lasts
    [SerializeField] Player caster;         // Who caused this effect
    [SerializeField] private Player owner;  // The owner of the effect
    TurnManager turnManager;                // Needed for checking current player

    public Player Caster { get => caster; set => caster = value; }
    public Player Owner { get => owner; set => owner = value; }

    private void Awake() {
        turnManager = GameObject.Find("TurnManager").GetComponent<TurnManager>();

        // TODO: set the owner for units and control points, tiles can't have an owner
        if(this.gameObject.GetComponent<Unit>() != null) {
            Owner = this.gameObject.GetComponent<Unit>().Owner;
        }
        else if (this.gameObject.GetComponent<ControlPoint>() != null) {
            Owner = this.gameObject.GetComponent<ControlPoint>().Owner;
        }
    }

    public void Activate(GameObject target, GameObject causer, object instigator) {
        bool effectShouldActivate = true;

        switch(effectSO.effectActivationType) {
            case EffectSO.EffectActivationType.AnyLastTurnStart:
            case EffectSO.EffectActivationType.AnyLastTurnEnd:
            case EffectSO.EffectActivationType.CasterLastTurnStart:
            case EffectSO.EffectActivationType.CasterLastTurnEnd:
            case EffectSO.EffectActivationType.EnemyLastTurnStart:
            case EffectSO.EffectActivationType.EnemyLastTurnEnd:
            case EffectSO.EffectActivationType.OwnerLastTurnStart:
            case EffectSO.EffectActivationType.OwnerLastTurnEnd:
                if (turnsInEffect != 1) {
                    effectShouldActivate = false;
                    turnsInEffect--;
                }
                break;
            default:
                break;
        }

        if (effectShouldActivate) {
            MessageManager.Show("'" + effectName + "' effect activates!");
            effectSO.Apply(target, causer, instigator);

            if (turnsInEffect != int.MaxValue) {
                turnsInEffect--;
            }

            if (turnsInEffect <= 0) {
                // Remove effect from the effect list:
                switch (effectSO.effectTargetType) {
                    case EffectSO.EffectTargetType.AnyUnit:
                    case EffectSO.EffectTargetType.EnemyUnit:
                    case EffectSO.EffectTargetType.FriendlyUnit:
                        Unit targetUnit = gameObject.GetComponent<Unit>();
                        targetUnit.effects.Remove(this);
                        break;
                    case EffectSO.EffectTargetType.AnyControlPoint:
                    case EffectSO.EffectTargetType.EnemyControlPoint:
                    case EffectSO.EffectTargetType.FriendlyControlPoint:
                        ControlPoint targetCP = gameObject.GetComponent<ControlPoint>();
                        targetCP.effects.Remove(this);
                        break;
                    case EffectSO.EffectTargetType.Tile:
                        Tile targetTile = gameObject.GetComponent<Tile>();
                        targetTile.effects.Remove(this);
                        break;
                }
                // Lastly, destroy the effect:
                Object.Destroy(this);
            }
        }
    }
}
