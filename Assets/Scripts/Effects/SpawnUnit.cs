﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "New Effect", menuName = "ScriptableObjects/Effects/SpawnUnit")]
public class SpawnUnit : EffectSO {
    [Header("SpawnUnit Variables")]
    public UnitCardSO unitToBeSpawned;
    public GameObject unitModel;
    public GameObject unitCardPrefab;

    public override void Apply(GameObject target, GameObject causer, object instigator) {

        Debug.LogWarning(target + ", " + causer + ", " + instigator);

        // Check if there's already a unit on the target tile:
        if (target.GetComponent<Tile>().UnitOnTop != null) {
            MessageManager.Show("Cannot spawn " + unitToBeSpawned.cardName + " on " + target.GetComponent<Tile>().ScriptableTile.tileName + " because there's a unit blocking it from spawning!");
            return;
        }
        else {
            // Create a unit card for the unit:
            GameObject card = Instantiate(unitCardPrefab, GameObject.Find("MainHUD").transform);
            card.GetComponent<UnitCard>().card = unitToBeSpawned;

            // If effect applies to a control point:
            if (target.GetComponent<ControlPoint>() != null) {
                card.GetComponent<Card>().Owner = target.GetComponent<ControlPoint>().Owner;
            }
            card.gameObject.name = "Card_" + unitToBeSpawned.name;
            card.GetComponent<UnitCard>().unitModel = unitModel;

            // Instantiate the unit:
            card.GetComponent<UnitCard>().SpawnUnit(target);

            // Place the card to the player's discard pile:
            card.GetComponent<Card>().Owner.playerDiscardPile.AddCardToDiscardPile(card);
        }
    }
}
