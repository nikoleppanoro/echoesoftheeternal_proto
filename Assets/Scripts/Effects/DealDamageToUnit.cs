﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "New Effect", menuName = "ScriptableObjects/Effects/DealDamage")]
public class DealDamageToUnit : EffectSO {
    [Header("DealDamage Variables")]
    public int damageAmount;

    public override void Apply(GameObject target, GameObject causer, object instigator) {
        Debug.Log("Target: " + target + ", causer: " + causer + ", instigator: " + instigator);

        // TODO: Get target unit
        Unit targetUnit = target.GetComponent<Tile>().UnitOnTop;

        // TODO: Deal damage to target unit:
        targetUnit.TakeDamage(damageAmount);
    }
}
