﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CardSO : ScriptableObject {
    [Header("Card Info")]
    public string cardName;
    public string cardType;
    public string cardRarity;
    public string flavorText;
    public string abilityText;
    public Image cardArtworkImage;

    [Header("Stats")]
    public int manaCost;
    public int specialManaCost;
}
