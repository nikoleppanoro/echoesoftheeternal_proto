﻿using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using TMPro;

// UnitCard and SpellCard classes inherit this class:
public class Card : MonoBehaviour, IDragHandler, IBeginDragHandler, IEndDragHandler {
    [Header("Other")]
    private Player owner;
    protected PlayerHand playerHand;
    protected Resources resource;
    protected Camera cam;
    protected float hudScale;
    protected bool cardWasPlayed;
    protected HUD hud;

    [Header("Card Data")]
    public Image artworkImage;
    public TextMeshProUGUI cardName;
    public TextMeshProUGUI infoText;
    public TextMeshProUGUI manaCostText;
    public TextMeshProUGUI flavorText;
    public TextMeshProUGUI abilityText;

    public CardSO card;

    public Player Owner { get => owner; set => owner = value; }

    protected void Start() {
        cam = GameObject.Find("Main Camera").GetComponent<Camera>();

        hudScale = GameObject.Find("MainHUD").transform.localScale.x;
        cardWasPlayed = false;

        playerHand = Owner.playerHand;
        resource = Owner.playerResources;
    }

    public void OnBeginDrag(PointerEventData eventData) {
        RemoveCardFromHand();
    }

    public void OnDrag(PointerEventData eventData) {   
        // Move card:
        // NOTE: Canvas Scaler messes stuff up when using UI Scale Mode "Scale With Screen Size",
        // works normally when using "Constant Pixel Size"
        // --> hudScale variable below fixes it (?) 
        transform.localPosition += (Vector3)eventData.delta / hudScale;
    }

    public void OnEndDrag(PointerEventData eventData) {
        // Do a raycast: if under a tile, spawn a unit on that tile, else return card to original position
        Ray ray = cam.ScreenPointToRay(Input.mousePosition);
        RaycastHit hit;

        if (Physics.Raycast(ray, out hit)) {
            GameObject objectHit = hit.transform.parent.gameObject;
            if (objectHit.GetComponent<Tile>() != null) {
                // Hit a hex tile:
                Debug.Log("Hit tile: " + objectHit.name);

                // Trade control point check:
                if(objectHit.GetComponent<ControlPoint>() != null) {
                    ControlPoint cp = objectHit.GetComponent<ControlPoint>();
                    // If the control point is owned by the player:
                    if (cp.Owner == Owner) {
                        // If the control point can trade cards:
                        if (cp.CanTradeCards) {
                            // If there is no cooldown:
                            if (cp.TradeCooldown <= 0) {
                                // Place the current card back to deck, shuffle it, and draw a new card:
                                Owner.playerDeck.AddCardToDeck(this.gameObject, true);
                                Owner.playerDeck.DrawCard(1);
                                // Also set a cooldown counter to control point:
                                cp.TradeCooldown = 3;
                                cp.CanTradeCards = false;
                                MessageManager.Show("The card has been returned to your deck and a new card was drawn.");
                                return;
                            }
                        }
                    }
                }

                // If the card is a unit card:
                if (gameObject.GetComponent<UnitCard>() != null) {
                    // Can we place a unit here?
                    if (objectHit.GetComponent<Tile>().CanSpawnUnits) {
                        // Is there already a unit on the tile?
                        if (objectHit.GetComponent<Tile>().UnitOnTop == null) {
                            // Is the tile a control point?
                            if (objectHit.GetComponent<ControlPoint>() != null) {
                                // Is the control point owned by the player?
                                if (objectHit.GetComponent<ControlPoint>().Owner == Owner) {
                                    // Does the player have enough mana?
                                    if (resource.StandardResCurrent >= card.manaCost && resource.SpecialResCurrent >= card.specialManaCost) {
                                        // Play the card:
                                        PlayCard();
                                        // Spawn the unit:
                                        SpawnUnit(objectHit);
                                    }
                                    else MessageManager.Show("You do not have enough resources to play this card!");
                                }
                                else MessageManager.Show("Can't summon unit here!");
                            }
                            //else Debug.Log("Not a control point!");
                        }
                        else MessageManager.Show("There's already a unit on that tile!");
                    }
                    else MessageManager.Show("Can't summon unit here!");
                }

                // If the card is a spell card:
                else if (gameObject.GetComponent<SpellCard>() != null) {
                    // Does the player have enough mana? 
                    if (resource.StandardResCurrent >= card.manaCost && resource.SpecialResCurrent >= card.specialManaCost) {
                        // Check if the target is valid for casting the spell:
                        if (CheckIfValidTarget(objectHit) == true) {
                            // Play the card:
                            PlayCard();
                            // Cast spell:
                            CastSpell(objectHit);
                        }
                    }
                    else MessageManager.Show("Not enough resources to play this card!");
                }
                else Debug.Log("Something went wrong...");
            }
        }
        // Lastly, move the card to the discard pile if it was played:
        if (cardWasPlayed) {
            Owner.playerDiscardPile.AddCardToDiscardPile(gameObject);
            Owner.cardInHand = null;
        }
        else {
            ReturnCardToHand();
        }
    }

    // Return card to hand (CardPanel) and set new card spacing:
    public void ReturnCardToHand() {
        transform.SetParent(playerHand.transform);
        playerHand.cardsInHand.Add(gameObject);
        playerHand.setCardSpacing();
        Owner.cardInHand = null;
    }

    // Remove card to hand (CardPanel) and set new card spacing:
    public void RemoveCardFromHand() {
        transform.SetParent(GameObject.Find("MainHUD").transform);
        playerHand.cardsInHand.Remove(gameObject);
        playerHand.setCardSpacing();
        Owner.cardInHand = this;
    }

    // Marks card as played and reduces player mana according to costs:
    public void PlayCard() {
        cardWasPlayed = true;
        resource.ReduceMana(card.manaCost);
        resource.ReduceSpecialMana(card.specialManaCost);
    }

    public virtual void SpawnUnit(GameObject obj) { }

    public virtual bool CheckIfValidTarget(GameObject target) { return false; } 

    public virtual void CastSpell(GameObject target) { }
}
