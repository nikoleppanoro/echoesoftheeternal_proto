﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class MessageManager : MonoBehaviour
{
    public static MessageManager instance;
    public GameObject messagePrefab;
    public GameObject messageParent;
    public List<GameObject> displayedMessages = new List<GameObject>();
    [SerializeField] List<GameObject> messageLog = new List<GameObject>();

    public static GameObject MessagePrefab { get => instance.messagePrefab; }
    public static GameObject MessageParent { get => instance.messageParent; }
    public static List<GameObject> DisplayedMessages { get => instance.displayedMessages; set => instance.displayedMessages = value; }

    private void Awake() {
        instance = this;
    }

    public static void Show(string message) {
        GameObject msg = Instantiate(MessagePrefab, MessageParent.transform);
        msg.GetComponent<TextMeshProUGUI>().SetText(message);
        msg.GetComponent<Message>().messageManager = instance;
        DisplayedMessages.Add(msg);
    }
    public static void Remove(GameObject msg) {
        DisplayedMessages.Remove(msg);
    }

    public static void ClearMessages() {
        if(DisplayedMessages.Count > 0) {
            for (int i = 0; i < DisplayedMessages.Count; i++) {
                Object.Destroy(DisplayedMessages[i]);
                //Messages.RemoveAt(i);
            }
            DisplayedMessages = new List<GameObject>();
        }
    }
}
