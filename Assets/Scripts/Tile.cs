﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

[SelectionBase]
public class Tile : MonoBehaviour {
    float tileHeight = 0.2f;

    [Header("Coordinates")]
    public int offsetCoordinateX;
    public int offsetCoordinateY;
    public int doubleWidthCoordinateX;
    public int doubleWidthCoordinateY;
    public int cubeCoordinateX;
    public int cubeCoordinateY;
    public int cubeCoordinateZ;

    [Header("Properties")]
    [SerializeField] int height = 1;
    [SerializeField] bool hasBeenExplored;
    [SerializeField] bool isVisible;
    [SerializeField] int visibility = 0;
    [SerializeField] bool canSpawnUnits;
    [SerializeField] bool isSelected = false;
    [SerializeField] float distance;
    [SerializeField] bool isInMovementRange;
    [SerializeField] bool isInAttackRange;
    [SerializeField] float highGroundMovementCost = 1;

    [Header("Effects")]
    public List<Effect> effects = new List<Effect>();

    [Header("Other")]
    [SerializeField] TileSO scriptableTile;
    GameObject hexTile;
    GameObject objectOnTop;
    [SerializeField] Unit unitOnTop;
    List<GameObject> neighbourTiles;
    GameObject currentHexTile;
    GameObject currentObjectOnTop;
    MeshRenderer tileMeshRenderer, objectMeshRenderer;
    Transform objectParent;
    public HexTileMap hexTileMap;
    public TextMesh textMesh;
    [SerializeField] GameObject fogTile;
    TurnManager turnManager;

    WaitForSeconds searchDelay = new WaitForSeconds(1 / 60f);
    List<Tile> frontier;

    public float Distance {
        get => distance;
        set {
            distance = value;
            UpdateDistanceText();
        }
    }
    public bool IsSelected {
        get {
            return isSelected;
        }
        set {
            isSelected = value;
            SetTileColor();
            GetNeighbours();
        }
    }
    public bool IsVisible {
        get => isVisible;
        set {
            isVisible = value;
            SetTileColor();
        }
    }
    public int Visibility { 
        get => visibility;
        set { 
            visibility = value;
            if (visibility == 0) {
                IsVisible = false;
                if (UnitOnTop != null) {
                    UnitOnTop.IsVisible = false;
                }
            }
            else if (visibility == 1) {
                IsVisible = true;
                if(UnitOnTop != null) {
                    UnitOnTop.IsVisible = true;
                }
            }
        }
    }
    public bool CanSpawnUnits { get => canSpawnUnits; set => canSpawnUnits = value; }
    public int Height {
        get => height;
        set {
            height = value;
            SetTileHeight();
        }
    }
    public TileSO ScriptableTile { 
        get => scriptableTile; 
        set => scriptableTile = value; 
    }
    public GameObject HexTile {
        get => hexTile;
        set {
            hexTile = value;
            PlaceHexTile(hexTile);
        }
    }
    public GameObject ObjectOnTop {
        get => objectOnTop;
        set {
            objectOnTop = value;
            SetObjectOnTop(objectOnTop);
        }
    }
    public Unit UnitOnTop {
        get => unitOnTop;
        set => unitOnTop = value;
    }
    public bool IsInMovementRange {
        get => isInMovementRange;
        set {
            isInMovementRange = value;
            SetTileColor();
        }
    }
    public bool IsInAttackRange { 
        get => isInAttackRange;
        set { 
            isInAttackRange = value;
            SetTileColor();
        } 
    }
    public List<GameObject> NeighbourTiles { 
        get => neighbourTiles; 
        set => neighbourTiles = value;
    }
    public bool HasBeenExplored {
        get => hasBeenExplored;
        set {
            hasBeenExplored = value;
            if(hasBeenExplored) {
                if(FogTile != null) {
                    FogTile.SetActive(false);
                }

                HexTile.GetComponent<MeshRenderer>().enabled = true;
                if (ObjectOnTop != null) {
                    ObjectOnTop.GetComponent<MeshRenderer>().enabled = true;
                }

                //Add the tile to current player's explored tiles list (if it's not there already):
                if(!hexTileMap.turnManager.CurrentPlayer.exploredTiles.Contains(this.gameObject)) {
                    hexTileMap.turnManager.CurrentPlayer.exploredTiles.Add(this.gameObject);
                }
            }
            else if (!hasBeenExplored) {
                if (FogTile != null) {
                    FogTile.SetActive(true);
                }

                HexTile.GetComponent<MeshRenderer>().enabled = false;
                if(ObjectOnTop != null) {
                    ObjectOnTop.GetComponent<MeshRenderer>().enabled = false;
                }
            }
        } 
    }
    public GameObject FogTile { get => fogTile; set => fogTile = value; }


    private void Start() {
        objectParent = GameObject.Find("Objects").transform;
        hexTileMap = GameObject.Find("HexTileMap").GetComponent<HexTileMap>();
        turnManager = GameObject.Find("TurnManager").GetComponent<TurnManager>();

        SetupEverything();

        // Fog of war enabled:
        if (hexTileMap.FogOfWarEnabled) {
            // Set tile as unexplored:
            HasBeenExplored = false;
            Visibility--;
        }
        // Darkness enabled:
        else if (hexTileMap.DarknessEnabled && !hexTileMap.startInEditMode) {
            HasBeenExplored = true;
            Visibility--;
        }
    }

#if UNITY_EDITOR
    private void OnValidate() {
        if (Application.isPlaying && PrefabUtility.GetPrefabAssetType(gameObject) == PrefabAssetType.NotAPrefab) {
            SetupEverything();
        }
    }
#endif

    private void OnEnable() {
        TurnManager.OnTurnStart += TurnStart;
        TurnManager.OnTurnEnd += TurnEnd;
    }

    private void OnDisable() {
        TurnManager.OnTurnStart -= TurnStart;
        TurnManager.OnTurnEnd -= TurnEnd;
    }

    private void SetupEverything() {
        SetTileValues();
        PlaceHexTile(hexTile);
        SetTileHeight();
        SetObjectOnTop(objectOnTop);
        SetTileColor();
        SetupDistanceText();

        if (ScriptableTile.name == "CP_PlayerKeep") {
            SetupPlayerKeepOwner();
        }
    }
    private void SetTileValues() {
        hexTile = ScriptableTile.tile;
        objectOnTop = ScriptableTile.objectOnTop;
    }
    public void PlaceHexTile(GameObject tile) {
        if (tile != currentHexTile) {
            if (currentHexTile != null) {
                Destroy(currentHexTile);
            }
            if (tile != null) {
                GameObject obj = Instantiate(tile, this.transform.position, Quaternion.identity, this.transform);
                hexTile = obj;
                currentHexTile = hexTile;
                tileMeshRenderer = hexTile.GetComponent<MeshRenderer>();
            }
        }
    }
    private void SetTileHeight() {
        if (height >= 1) {
            gameObject.transform.localPosition = new Vector3(transform.localPosition.x, 0, transform.localPosition.z);
            gameObject.transform.localScale = new Vector3(transform.localScale.x, height, transform.localScale.z);
        }
        if (height < 1) {
            gameObject.transform.localScale = Vector3.one;
            gameObject.transform.localPosition = new Vector3(transform.localPosition.x, (height * tileHeight) - tileHeight, transform.localPosition.z);
        }
    }
    private void SetObjectOnTop(GameObject value) {
        // If object on top changed:
        if (objectOnTop != currentObjectOnTop) {
            if (currentObjectOnTop != null) {
                Destroy(currentObjectOnTop);
                objectMeshRenderer = null;
            }
            if (objectOnTop != null) {
                GameObject obj = Instantiate(value, this.transform.position + new Vector3(0, (height * tileHeight) - tileHeight, 0), Quaternion.identity, objectParent);
                objectOnTop = obj;
                currentObjectOnTop = objectOnTop;
                objectMeshRenderer = currentObjectOnTop.GetComponentInChildren<MeshRenderer>();
            }
        }
        // If tile height changed:
        else if (objectOnTop != null && objectOnTop == currentObjectOnTop) {
            objectOnTop.transform.position = this.transform.position + new Vector3(0, (height * tileHeight) - tileHeight, 0);
        }
    }
    private void SetupDistanceText() {
        GameObject text = new GameObject("DistanceText");
        textMesh = text.AddComponent<TextMesh>();
        text.gameObject.transform.parent = this.gameObject.transform;
        text.gameObject.transform.localPosition = Vector3.zero;
        text.gameObject.transform.localScale = new Vector3(0.1f, 0.1f / height, 1f);
        textMesh.fontSize = 50;
        textMesh.anchor = TextAnchor.LowerCenter;
        textMesh.text = "";
    }
    
    // Changes the color of the tile depending on tile values (changing color creates a lot of material instances, could this be optimized?)
    private void SetTileColor() {
        // Tile is visible:
        if (IsVisible) {
            if (!IsSelected) {
                tileMeshRenderer.material.color = hexTileMap.tileColors[0].color;
                //tileMeshRenderer.material = tileMeshRenderer.sharedMaterial;
            }
            else if (IsSelected) {
                tileMeshRenderer.material.color = hexTileMap.tileColors[1].color;
            }
        }
        // Tile is NOT visible:
        else if (!IsVisible) {
            if(!IsSelected) {
                tileMeshRenderer.material.color = hexTileMap.tileColors[2].color;
            }
            else if(IsSelected) {
                tileMeshRenderer.material.color = hexTileMap.tileColors[3].color;
            }
        }
        // Tile is in movement range:
        if (IsInMovementRange && !IsInAttackRange) {
            tileMeshRenderer.material.color = hexTileMap.tileColors[4].color;
        }
        // Tile is in attack range:
        else if (!IsInMovementRange && IsInAttackRange) {
            tileMeshRenderer.material.color = hexTileMap.tileColors[5].color;
        }
        // Tile is in movement AND attack range:
        else if (IsInMovementRange && IsInAttackRange) {
            tileMeshRenderer.material.color = hexTileMap.tileColors[6].color;
        }

        //Change object on top color as well if it's not a control point:
        if (ObjectOnTop != null && gameObject.GetComponent<ControlPoint>() == null) {
            if(IsVisible) {
                if (!IsSelected) {
                    objectMeshRenderer.material.color = hexTileMap.tileColors[0].color;
                }
                else if (IsSelected) {
                    objectMeshRenderer.material.color = hexTileMap.tileColors[1].color;
                }
            }
            else if(!IsVisible) {
                if (!IsSelected) {
                    objectMeshRenderer.material.color = hexTileMap.tileColors[2].color;
                }
                else if (IsSelected) {
                    objectMeshRenderer.material.color = hexTileMap.tileColors[3].color;
                }
            }
        }
    }
    public void SetupPlayerKeepOwner() {
        try {
            gameObject.GetComponent<ControlPoint>().Owner = GameObject.Find("TurnManager").GetComponent<TurnManager>().Players[TurnManager.PlayerIndex];
            TurnManager.PlayerIndex++;
        }
        catch (NullReferenceException e) {
            Debug.LogWarning(e);
        }

    }
    public void GetNeighbours() {
        NeighbourTiles = new List<GameObject>();
        int[,] doubleWidthDirections = new int[,] {{2, 0}, {1, -1}, {-1, -1}, {-2, 0}, {-1, 1}, {1, 1}};

        int parity = doubleWidthCoordinateY & 1;

        for (int i = 0; i < 6; i++) {
            int[] dir = new int[] { doubleWidthDirections[i, 0], doubleWidthDirections[i, 1]};
            GameObject obj = GameObject.Find("Hex_" + (doubleWidthCoordinateX + dir[0]) + "_" + (doubleWidthCoordinateY + dir[1]));
            if(obj != null) {
                NeighbourTiles.Add(obj);
            }
        }
    }
    public void ShowDistances() {
        Debug.Log("Showing distances...");
        StopAllCoroutines();
        StartCoroutine(MovementSearch(this));
    }
    public void ShowAttackRange() {
        Debug.Log("Showing attack range...");
        //StopAllCoroutines();
        GetTilesInAttackRange();
    }
    public int DistanceTo(Tile other) {
        return
            ((cubeCoordinateX < other.cubeCoordinateX ? other.cubeCoordinateX - cubeCoordinateX : cubeCoordinateX - other.cubeCoordinateX) +
            (cubeCoordinateY < other.cubeCoordinateY ? other.cubeCoordinateY - cubeCoordinateY : cubeCoordinateY - other.cubeCoordinateY) +
            (cubeCoordinateZ < other.cubeCoordinateZ ? other.cubeCoordinateZ - cubeCoordinateZ : cubeCoordinateZ - other.cubeCoordinateZ)) / 2;
    }
    public void UpdateDistanceText() {
        if(Distance == int.MaxValue || Distance == 0) {
            textMesh.text = "";
            IsInMovementRange = false;
        }
        else {
            textMesh.text = Distance.ToString();
            IsInMovementRange = true;
            MakeTextLookAtACamera();
        }
    }
    void MakeTextLookAtACamera() {
        //Make distance text to look at camera:
        textMesh.transform.rotation = Quaternion.LookRotation(transform.position - Camera.main.transform.position);
    }
    IEnumerator MovementSearch(Tile tile) {
        hexTileMap.ResetTileDistances();
        frontier = new List<Tile>();
        Distance = 0;
        frontier.Add(tile);

        while (frontier.Count > 0) {
            //yield return delay;
            Tile current = frontier[0];
            frontier.RemoveAt(0);

            string unitType = UnitOnTop.GetComponent<Unit>().UnitType;

            // Loop through neighbours:
            foreach (GameObject neighbour in current.NeighbourTiles) {
                Tile neighbourTile = neighbour.GetComponent<Tile>();

                // Unit is both land and sea unit:
                if (unitType.Contains("Land") && unitType.Contains("Water")) {
                    // Skip tile if...
                    // Tile has not been explored yet:
                    if (neighbourTile.HasBeenExplored == false) {
                        continue;
                    }
                    // ...tile blocks land and sea movement (mountains):
                    if (neighbourTile.ScriptableTile.blocksLandMovement == true && neighbourTile.ScriptableTile.blocksSeaMovement == true) {
                        continue;
                    }
                    // ...height difference is 2 or more:
                    if ((neighbourTile.Height - current.Height) >= 2) {
                        continue;
                    }
                    // ...movement cost is higher than unit current movement:
                    if ((current.Distance + neighbourTile.ScriptableTile.movementCost) > UnitOnTop.GetComponent<Unit>().MovementCurrent) {
                        continue;
                    }
                    // ...tile has a control point...
                    if(neighbourTile.GetComponent<ControlPoint>() != null) {
                        //.. that hasn't been captured yet:
                        if(neighbourTile.GetComponent<ControlPoint>().Owner != UnitOnTop.GetComponent<Unit>().Owner) {
                            continue;
                        }
                    }
                    // If there's a unit on the tile:
                    if (neighbourTile.UnitOnTop != null) {
                        // Enemy units block the way: 
                        if (neighbourTile.UnitOnTop.GetComponent<Unit>().Owner != UnitOnTop.GetComponent<Unit>().Owner) {
                            // But only if they're visible:
                            if (neighbourTile.UnitOnTop.IsVisible) continue;
                        }
                    }

                    // High ground check:
                    bool isOnHighGround = false;
                    if (neighbourTile.Height > current.Height) {
                        isOnHighGround = true;
                    }

                    // Distance not assigned yet:
                    if (neighbourTile.Distance == int.MaxValue) {
                        if (!isOnHighGround) {
                            neighbourTile.Distance = current.Distance + neighbourTile.ScriptableTile.movementCost;
                            // Tile is reachable:
                            if (neighbourTile.Distance <= UnitOnTop.GetComponent<Unit>().MovementCurrent) {
                                AddToFrontier(neighbourTile);
                            }
                            // Tile is not reachable:
                            else if (neighbourTile.Distance > UnitOnTop.GetComponent<Unit>().MovementCurrent) {
                                neighbourTile.Distance = int.MaxValue;
                            }
                        }
                        else {
                            neighbourTile.Distance = current.Distance + neighbourTile.ScriptableTile.movementCost + highGroundMovementCost;
                            // Tile is reachable:
                            if (neighbourTile.Distance <= UnitOnTop.GetComponent<Unit>().MovementCurrent) {
                                AddToFrontier(neighbourTile);
                            }
                            // Tile is not reachable:
                            else if (neighbourTile.Distance > UnitOnTop.GetComponent<Unit>().MovementCurrent) {
                                neighbourTile.Distance = int.MaxValue;
                            }
                        }
                    }
                    // Distance assigned, is there a shorter path available:
                    else {
                        if (!isOnHighGround) {
                            if (current.Distance < (neighbourTile.Distance - neighbourTile.ScriptableTile.movementCost)) {
                                neighbourTile.Distance = current.Distance + neighbourTile.ScriptableTile.movementCost;
                            }
                        }
                        else {
                            if (current.Distance < (neighbourTile.Distance - neighbourTile.ScriptableTile.movementCost - highGroundMovementCost)) {
                                neighbourTile.Distance = current.Distance + neighbourTile.ScriptableTile.movementCost + highGroundMovementCost;
                            }
                        }
                    }
                }

                // Unit is a land unit:
                else if (unitType.Contains("Land")) {
                    // Skip tile if...
                    // Tile has not been explored yet:
                    if (neighbourTile.HasBeenExplored == false) {
                        continue;
                    }
                    // ...tile blocks land movement:
                    if (neighbourTile.ScriptableTile.blocksLandMovement == true) {
                        continue;
                    }
                    // ...height difference is 2 or more:
                    if ((neighbourTile.Height - current.Height) >= 2) {
                        continue;
                    }
                    // ...movement cost is higher than unit current movement:
                    if ((current.Distance + neighbourTile.ScriptableTile.movementCost) > UnitOnTop.GetComponent<Unit>().MovementCurrent) {
                        continue;
                    }
                    // ...tile has a control point...
                    if (neighbourTile.GetComponent<ControlPoint>() != null) {
                        //.. that hasn't been captured yet:
                        if (neighbourTile.GetComponent<ControlPoint>().Owner != UnitOnTop.GetComponent<Unit>().Owner) {
                            continue;
                        }
                    }
                    // If there's a unit on the tile:
                    if (neighbourTile.UnitOnTop != null) {
                        // Enemy units block the way: 
                        if (neighbourTile.UnitOnTop.GetComponent<Unit>().Owner != UnitOnTop.GetComponent<Unit>().Owner) {
                            // But only if they're visible:
                            if (neighbourTile.UnitOnTop.IsVisible) continue;
                        }
                    }

                    // High ground check:
                    bool isOnHighGround = false;
                    if(neighbourTile.Height > current.Height) {
                        isOnHighGround = true;
                    }

                    // Distance not assigned yet:
                    if (neighbourTile.Distance == int.MaxValue) {
                        if(!isOnHighGround) {
                            neighbourTile.Distance = current.Distance + neighbourTile.ScriptableTile.movementCost;
                            // Tile is reachable:
                            if (neighbourTile.Distance <= UnitOnTop.GetComponent<Unit>().MovementCurrent) {
                                AddToFrontier(neighbourTile);
                            }
                            // Tile is not reachable:
                            else if (neighbourTile.Distance > UnitOnTop.GetComponent<Unit>().MovementCurrent) {
                                neighbourTile.Distance = int.MaxValue;
                            }
                        }
                        else {
                            neighbourTile.Distance = current.Distance + neighbourTile.ScriptableTile.movementCost + highGroundMovementCost;
                            // Tile is reachable:
                            if (neighbourTile.Distance <= UnitOnTop.GetComponent<Unit>().MovementCurrent) {
                                AddToFrontier(neighbourTile);
                            }
                            // Tile is not reachable:
                            else if (neighbourTile.Distance > UnitOnTop.GetComponent<Unit>().MovementCurrent) {
                                neighbourTile.Distance = int.MaxValue;
                            }
                        }
                    }
                    // Distance assigned, is there a shorter path available:
                    else {
                        if (!isOnHighGround) {
                            if (current.Distance < (neighbourTile.Distance - neighbourTile.ScriptableTile.movementCost)) {
                                neighbourTile.Distance = current.Distance + neighbourTile.ScriptableTile.movementCost;
                            }
                        }
                        else {
                            if (current.Distance < (neighbourTile.Distance - neighbourTile.ScriptableTile.movementCost - highGroundMovementCost)) {
                                neighbourTile.Distance = current.Distance + neighbourTile.ScriptableTile.movementCost + highGroundMovementCost;
                            }
                        }
                    }
                }

                // Unit is a flying unit:
                else if (unitType.Contains("Flying")) {
                    // Skip tile if...
                    // Tile has not been explored yet:
                    if (neighbourTile.HasBeenExplored == false) {
                        continue;
                    }
                    // ...movement cost is higher than unit current movement:
                    if ((current.Distance + 1) > unitOnTop.GetComponent<Unit>().MovementCurrent) {
                        continue;
                    }
                    // ...tile has a control point...
                    if (neighbourTile.GetComponent<ControlPoint>() != null) {
                        // ...that hasn't been captured yet:
                        if (neighbourTile.GetComponent<ControlPoint>().Owner != UnitOnTop.GetComponent<Unit>().Owner) {
                            continue;
                        }
                    }
                    // If there's a unit on the tile:
                    if (neighbourTile.UnitOnTop != null) {
                        // Enemy units block the way: 
                        if (neighbourTile.UnitOnTop.GetComponent<Unit>().Owner != UnitOnTop.GetComponent<Unit>().Owner) {
                            // But only if they're visible:
                            if (neighbourTile.UnitOnTop.IsVisible) continue;
                        }
                    }

                    // Distance not assigned yet:
                    if (neighbourTile.Distance == int.MaxValue) {
                        neighbourTile.Distance = current.Distance + 1;
                        if (neighbourTile.Distance <= unitOnTop.GetComponent<Unit>().MovementCurrent) {
                            AddToFrontier(neighbourTile);
                        }
                    }
                }

                // Unit is a water unit:
                else if (unitType.Contains("Water")) {
                    // Skip tile if...
                    // Tile has not been explored yet:
                    if (neighbourTile.HasBeenExplored == false) {
                        continue;
                    }
                    // ...tile blocks sea movement:
                    if (neighbourTile.ScriptableTile.blocksSeaMovement == true) {
                        continue;
                    }
                    // ...movement cost is higher than unit current movement:
                    if ((current.Distance + neighbourTile.ScriptableTile.movementCost) > unitOnTop.GetComponent<Unit>().MovementCurrent) {
                        continue;
                    }
                    // ...tile has a control point...
                    if (neighbourTile.GetComponent<ControlPoint>() != null) {
                        // ...that hasn't been captured yet:
                        if (neighbourTile.GetComponent<ControlPoint>().Owner != UnitOnTop.GetComponent<Unit>().Owner) {
                            continue;
                        }
                    }
                    // If there's a unit on the tile:
                    if (neighbourTile.UnitOnTop != null) {
                        // Enemy units block the way: 
                        if (neighbourTile.UnitOnTop.GetComponent<Unit>().Owner != UnitOnTop.GetComponent<Unit>().Owner) {
                            // But only if they're visible:
                            if (neighbourTile.UnitOnTop.IsVisible) continue;
                        }
                    }

                    // Distance not assigned yet:
                    if (neighbourTile.Distance == int.MaxValue) {
                        neighbourTile.Distance = current.Distance + neighbourTile.ScriptableTile.movementCost;
                        if (neighbourTile.Distance <= unitOnTop.GetComponent<Unit>().MovementCurrent) {
                            AddToFrontier(neighbourTile);
                        }
                    }
                }

                // Sort frontier:
                frontier.Sort((x, y) => x.Distance.CompareTo(y.Distance));
            }
        }
        yield return null;
    }
    void AddToFrontier(Tile tile) {
        frontier.Add(tile);
        if (!hexTileMap.tilesInMovementRange.Contains(tile)) {
            hexTileMap.tilesInMovementRange.Add(tile);
        }
    }
    void GetTilesInAttackRange() {
        hexTileMap.ResetAttackRange();

        int minRange = UnitOnTop.GetComponent<Unit>().AttackRangeMin;
        int maxRange = UnitOnTop.GetComponent<Unit>().AttackRangeMax;

        Debug.Log("Range: " + minRange + "-" + maxRange);

        foreach (GameObject obj in hexTileMap.tiles) {
            int tileDistance = DistanceTo(obj.GetComponent<Tile>());

            // Ignore melee range tile if height difference is more than 2 units:
            if(UnitOnTop.GetComponent<Unit>().AttackRangeMax == 1) {
                if (tileDistance == 1 && Math.Abs((obj.GetComponent<Tile>().Height - Height)) >= 2) {
                    continue;
                }
            }

            // If tile is within unit attack range and is visible:
            if (tileDistance >= minRange && tileDistance <= maxRange && tileDistance != 0 && obj.GetComponent<Tile>().IsVisible) {
                obj.GetComponent<Tile>().IsInAttackRange = true;
                if (!hexTileMap.tilesInAttackRange.Contains(obj.GetComponent<Tile>())) {
                    hexTileMap.tilesInAttackRange.Add(obj.GetComponent<Tile>());
                }
            }
        }
    }
    public void IncreaseVisibilityInRange(Unit unit) {
        if(hexTileMap.FogOfWarEnabled || hexTileMap.DarknessEnabled) {
            //Debug.Log("Increasing visibility with unit vision range of " + unit.Vision + " on " + gameObject.name);

            // Get all tiles in unit vision range:
            List<Tile> tilesInRange = GetTilesInRange(unit.Vision, null);
            // Loop through each tile:
            foreach (Tile tile in tilesInRange) {
                // Draw a line from this tile to target tile:
                List<Tile> tilesInLine = DrawLine(this, tile);
                // Go through each tile in order and determine visibility:
                for (int i = 0; i < tilesInLine.Count; i++) {
                    int tileDistance = DistanceTo(tilesInLine[i]);
                    // Center tile:
                    if (tileDistance == 0) {
                        tilesInLine[i].IncreaseVisibility();
                        continue;
                    }
                    // Neighbour tiles:
                    if (tileDistance == 1) {
                        // Increase visibility only if tile height difference is less than 2 or the unit is flying type:
                        if (tilesInLine[i].Height - this.Height < 2 || unit.scriptableUnitCard.cardType.Contains("Flying")) {
                            tilesInLine[i].IncreaseVisibility();
                        }
                        // If tile height is 2 or more, mark tile as explored:
                        else if (tilesInLine[i].Height - this.Height >= 2) {
                            tilesInLine[i].HasBeenExplored = true;
                        }
                        // Stop if tile blocks further vision:
                        if (tilesInLine[i].ScriptableTile.blocksLineOfSight && tilesInLine[i].Height >= this.Height) {
                            break;
                        }
                        // Stop if tile is higher than this tile:
                        else if (tilesInLine[i].Height > this.Height) {
                            break;
                        }
                        else {
                            continue;
                        }
                    }
                    // Rest of the tiles:
                    else {
                        // If forest or mountain:
                        if (tilesInLine[i].ScriptableTile.blocksLineOfSight) {
                            // If mountain, increase visibility for that tile:
                            if (tilesInLine[i].ScriptableTile.tileName.Contains("Mountain")) tilesInLine[i].IncreaseVisibility();
                            // Mark tile as explored:
                            if (tilesInLine[i].HasBeenExplored == false) {
                                tilesInLine[i].HasBeenExplored = true;
                            }
                            // Stop if tile height is higher or equal than this tile:
                            if (tilesInLine[i].Height >= this.Height) {
                                break;
                            }
                            else {
                                continue;
                            }
                        }
                        // If tile height is higher than this tile:
                        else if (tilesInLine[i].Height > this.Height) {
                            // Mark tile as explored:
                            if (tilesInLine[i].HasBeenExplored == false) {
                                tilesInLine[i].HasBeenExplored = true;
                            }
                            break;
                        }
                        else tilesInLine[i].IncreaseVisibility();
                    }
                }
            }
        }
    }
    public void IncreaseVisibilityInRange(int range) {
        if (hexTileMap.FogOfWarEnabled || hexTileMap.DarknessEnabled) {
            //Debug.Log("Increasing visibility with range of " + range + " on " + gameObject.name);

            // Get all tiles in unit vision range:
            List<Tile> tilesInRange = GetTilesInRange(range, null);
            // Loop through each tile:
            foreach (Tile tile in tilesInRange) {
                // Draw a line from this tile to target tile:
                List<Tile> tilesInLine = DrawLine(this, tile);
                // Go through each tile in order and determine visibility:
                for (int i = 0; i < tilesInLine.Count; i++) {
                    int tileDistance = DistanceTo(tilesInLine[i]);
                    // Center tile:
                    if (tileDistance == 0) {
                        tilesInLine[i].IncreaseVisibility();
                        continue;
                    }
                    // Neighbour tiles:
                    if (tileDistance == 1) {
                        // Increase visibility only if tile height difference is less than 2:
                        if (tilesInLine[i].Height - this.Height < 2) {
                            tilesInLine[i].IncreaseVisibility();
                        }
                        // If tile height is 2 or more, mark tile as explored:
                        else if (tilesInLine[i].Height - this.Height >= 2) {
                            tilesInLine[i].HasBeenExplored = true;
                        }
                        // Stop if tile blocks further vision:
                        if (tilesInLine[i].ScriptableTile.blocksLineOfSight && tilesInLine[i].Height >= this.Height) {
                            break;
                        }
                        // Stop if tile is higher than this tile:
                        else if (tilesInLine[i].Height > this.Height) {
                            break;
                        }
                        else {
                            continue;
                        }
                    }
                    // Rest of the tiles:
                    else {
                        // If forest or mountain:
                        if (tilesInLine[i].ScriptableTile.blocksLineOfSight) {
                            // If mountain, increase visibility for that tile:
                            if(tilesInLine[i].ScriptableTile.tileName.Contains("Mountain")) tilesInLine[i].IncreaseVisibility();
                            // Mark tile as explored:
                            if (tilesInLine[i].HasBeenExplored == false) {
                                tilesInLine[i].HasBeenExplored = true;
                            }
                            // Stop if tile height is higher or equal than this tile:
                            if (tilesInLine[i].Height >= this.Height) {
                                break;
                            }
                            else {
                                continue;
                            }
                        }
                        // If tile height is higher than this tile:
                        else if (tilesInLine[i].Height > this.Height) {
                            // Mark tile as explored:
                            if (tilesInLine[i].HasBeenExplored == false) {
                                tilesInLine[i].HasBeenExplored = true;
                            }
                            break;
                        }
                        else tilesInLine[i].IncreaseVisibility();
                    }
                }
            }
        }
    }
    public void DecreaseVisibilityInRange(Unit unit) {
        if (hexTileMap.FogOfWarEnabled || hexTileMap.DarknessEnabled) {
            //Debug.Log("Decreasing visibility with unit vision range of " + unit.Vision + " on " + gameObject.name);

            // Get all tiles in unit vision range:
            List<Tile> tilesInRange = GetTilesInRange(unit.Vision, null);
            // Loop through each tile:
            foreach (Tile tile in tilesInRange) {
                // Draw a line from this tile to target tile:
                List<Tile> tilesInLine = DrawLine(this, tile);
                // Go through each tile in order and determine visibility:
                for (int i = 0; i < tilesInLine.Count; i++) {
                    int tileDistance = DistanceTo(tilesInLine[i]);
                    // Center tile:
                    if (tileDistance == 0) {
                        tilesInLine[i].DecreaseVisibility();
                        continue;
                    }
                    // Neighbour tiles:
                    if (tileDistance == 1) {
                        // Decrease visibility only if tile height difference is less than 2 or the unit is flying type:
                        if (tilesInLine[i].Height - this.Height < 2 || unit.scriptableUnitCard.cardType.Contains("Flying")) {
                            tilesInLine[i].DecreaseVisibility();
                        }
                        // Stop if tile blocks further vision:
                        if (tilesInLine[i].ScriptableTile.blocksLineOfSight && tilesInLine[i].Height >= this.Height) {
                            break;
                        }
                        // Stop if tile is higher than this tile:
                        else if (tilesInLine[i].Height > this.Height) {
                            break;
                        }
                        else {
                            continue;
                        }
                    }
                    // Rest of the tiles:
                    else {
                        // If forest or mountain:
                        if (tilesInLine[i].ScriptableTile.blocksLineOfSight) {
                            // If mountain, decrease visibility for that tile:
                            if (tilesInLine[i].ScriptableTile.tileName.Contains("Mountain")) tilesInLine[i].DecreaseVisibility();
                            // Stop if tile height is higher or equal than this tile:
                            if (tilesInLine[i].Height >= this.Height) {
                                break;
                            }
                            else {
                                continue;
                            }
                        }
                        // If tile height is higher than this tile:
                        else if (tilesInLine[i].Height > this.Height) break;
                        else tilesInLine[i].DecreaseVisibility();
                    }
                }
            }
        }
    }
    public void DecreaseVisibilityInRange(int range) {
        if (hexTileMap.FogOfWarEnabled || hexTileMap.DarknessEnabled) {
            //Debug.Log("Decreasing visibility in range of " + range + " on " + gameObject.name);

            // Get all tiles in unit vision range:
            List<Tile> tilesInRange = GetTilesInRange(range, null);
            // Loop through each tile:
            foreach (Tile tile in tilesInRange) {
                // Draw a line from this tile to target tile:
                List<Tile> tilesInLine = DrawLine(this, tile);
                // Go through each tile in order and determine visibility:
                for (int i = 0; i < tilesInLine.Count; i++) {
                    int tileDistance = DistanceTo(tilesInLine[i]);
                    // Center tile:
                    if (tileDistance == 0) {
                        tilesInLine[i].DecreaseVisibility();
                        continue;
                    }
                    // Neighbour tiles:
                    if (tileDistance == 1) {
                        // Decrease visibility only if tile height difference is less than 2:
                        if (tilesInLine[i].Height - this.Height < 2) {
                            tilesInLine[i].DecreaseVisibility();
                        }
                        // Stop if tile blocks further vision:
                        if (tilesInLine[i].ScriptableTile.blocksLineOfSight && tilesInLine[i].Height >= this.Height) {
                            break;
                        }
                        // Stop if tile is higher than this tile:
                        else if (tilesInLine[i].Height > this.Height) {
                            break;
                        }
                        else {
                            continue;
                        }
                    }
                    // Rest of the tiles:
                    else {
                        // If forest or mountain:
                        if (tilesInLine[i].ScriptableTile.blocksLineOfSight) {
                            // If mountain, decrease visibility for that tile:
                            if (tilesInLine[i].ScriptableTile.tileName.Contains("Mountain")) tilesInLine[i].DecreaseVisibility();
                            // Stop if tile height is higher or equal than this tile:
                            if (tilesInLine[i].Height >= this.Height) {
                                break;
                            }
                            else {
                                continue;
                            }
                        }
                        // If tile height is higher than this tile:
                        else if (tilesInLine[i].Height > this.Height) break;
                        else tilesInLine[i].DecreaseVisibility();
                    }
                }
            }
        }
    }
    List<Tile> DrawLine(Tile startTile, Tile endTile) {
        List<Tile> tilesInLine = new List<Tile>();
        int tileDistance = DistanceTo(endTile);

        if (tileDistance >= 1) {
            for (float i = 0; i <= tileDistance; i++) {
                Vector3 point = new Vector3(
                    Mathf.Lerp(startTile.transform.position.x, endTile.transform.position.x, (i / tileDistance)),
                    Mathf.Lerp(startTile.transform.position.y, endTile.transform.position.y, (i / tileDistance)),
                    Mathf.Lerp(startTile.transform.position.z, endTile.transform.position.z, (i / tileDistance)));

                Tile tileOnPoint = FindTileInPos(point);

                if(tileOnPoint != null) {
                    tilesInLine.Add(tileOnPoint);
                }
            }
        }
        return tilesInLine;
    }
    public void IncreaseVisibility() {
        Visibility += 1;
        if (!hasBeenExplored) {
            HasBeenExplored = true;
        }
    }
    public void DecreaseVisibility() {
        Visibility -= 1;
    }
    public List<Tile> GetTilesInRange(int range, TileSO tileType) {
        List<Tile> tilesInRange = new List<Tile>();

        foreach (GameObject tile in hexTileMap.tiles) {
            int tileDistance = DistanceTo(tile.GetComponent<Tile>());
            if (tileDistance <= range) {
                // Tile type has been specified:                
                if (tileType == null) {
                    tilesInRange.Add(tile.GetComponent<Tile>());
                }
                // Only add if tile type is the same as specified:
                if (tileType != null && tile.GetComponent<Tile>().ScriptableTile == tileType)
                tilesInRange.Add(tile.GetComponent<Tile>());
            }
        }
        return tilesInRange;
    }
    Tile FindTileInPos(Vector3 pos) {
        Tile hitTile;
        RaycastHit hit;
        Ray ray = new Ray(new Vector3(pos.x, pos.y + 5, pos.z), Vector3.down);

        // If we hit something:
        if (Physics.Raycast(ray, out hit)) {
            hitTile = hit.transform.parent.gameObject.GetComponent<Tile>();
            // Hex tile hit:
            if (hitTile != null) {
                return hitTile;
            }
            else return null;
        }
        else return null;

    }
    void TurnStart() {
        ApplyTurnStartEffects();
    }
    void TurnEnd() {
        ApplyTurnEndEffects();
    }
    void ApplyTurnStartEffects() {
        // Apply effects that activate at start of turn:
        if (effects.Count > 0) {
            for (int i = 0; i < effects.Count; i++) {
                switch (effects[i].effectSO.effectActivationType) {
                    case EffectSO.EffectActivationType.AnyTurnStart:
                    case EffectSO.EffectActivationType.AnyLastTurnStart:
                        effects[i].Activate(gameObject, gameObject, this);
                        break;
                    case EffectSO.EffectActivationType.CasterTurnStart:
                    case EffectSO.EffectActivationType.CasterLastTurnStart:
                        if (turnManager.CurrentPlayer == effects[i].Caster)
                            effects[i].Activate(gameObject, gameObject, this);
                        break;
                    case EffectSO.EffectActivationType.EnemyTurnStart:
                    case EffectSO.EffectActivationType.EnemyLastTurnStart:
                        if (turnManager.CurrentPlayer != effects[i].Caster)
                            effects[i].Activate(gameObject, gameObject, this);
                        break;
                    case EffectSO.EffectActivationType.OwnerTurnStart:
                    case EffectSO.EffectActivationType.OwnerLastTurnStart:
                        if (effects[i].Owner == turnManager.CurrentPlayer)
                            effects[i].Activate(gameObject, gameObject, this);
                        break;
                    default:
                        break;
                }
            }
        }
    }
    void ApplyTurnEndEffects() {
        // Apply effects that activate at the end of turn:
        if (effects.Count > 0) {
            for (int i = 0; i < effects.Count; i++) {
                switch (effects[i].effectSO.effectActivationType) {
                    case EffectSO.EffectActivationType.AnyTurnEnd:
                    case EffectSO.EffectActivationType.AnyLastTurnEnd:
                        effects[i].Activate(gameObject, gameObject, this);
                        break;
                    case EffectSO.EffectActivationType.CasterTurnEnd:
                    case EffectSO.EffectActivationType.CasterLastTurnEnd:
                        if (turnManager.CurrentPlayer == effects[i].Caster)
                            effects[i].Activate(gameObject, gameObject, this);
                        break;
                    case EffectSO.EffectActivationType.EnemyTurnEnd:
                    case EffectSO.EffectActivationType.EnemyLastTurnEnd:
                        if (turnManager.CurrentPlayer != effects[i].Caster)
                            effects[i].Activate(gameObject, gameObject, this);
                        break;
                    case EffectSO.EffectActivationType.OwnerTurnEnd:
                    case EffectSO.EffectActivationType.OwnerLastTurnEnd:
                        if (effects[i].Owner == turnManager.CurrentPlayer)
                            effects[i].Activate(gameObject, gameObject, this);
                        break;
                    default:
                        break;
                }
            }
        }
    }
}