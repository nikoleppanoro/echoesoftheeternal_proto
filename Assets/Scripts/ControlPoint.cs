﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using UnityEngine;

public class ControlPoint : MonoBehaviour {
    Tile tile;
    TurnManager turnManager;

    [Header("Properties")]
    [SerializeField] Player owner;
    [SerializeField] ControlPointSO controlPointSO;
    [SerializeField] int standardResPerTurn;
    [SerializeField] int specialResPerTurn;
    [SerializeField] int visibilityRange;
    [SerializeField] bool canSpawnUnits;
    [SerializeField] bool canTradeCards;
    [SerializeField] bool resourceGainsAllowed;
    [SerializeField] int tradeCooldown = 0;

    [Header("Capturing")]
    [SerializeField] int captureCounter = 0;
    [SerializeField] int captureTicksRequired;
    [SerializeField] Player playerCapturing;
    [SerializeField] List<Unit> nearbyUnits;

    [Header("Effects")]
    public List<Effect> effects = new List<Effect>();

    public Tile Tile { get => tile; set => tile = value; }
    public Player Owner {
        get => owner;
        set {
            if (owner == null) {
                owner = value;
                if (Tile.ObjectOnTop != null) {
                    OnOwnerChanged();
                }
            }
            else if (owner != null) {
                owner.ownedControlPoints.Remove(this);
                owner = value;
                if (Tile.ObjectOnTop != null) {
                    OnOwnerChanged();
                }
            }
        }
    }
    public ControlPointSO ControlPointSO { get => controlPointSO; set => controlPointSO = value; }
    public int StandardResPerTurn { get => standardResPerTurn; set => standardResPerTurn = value; }
    public int SpecialResPerTurn { get => specialResPerTurn; set => specialResPerTurn = value; }
    public int VisibilityRange { get => visibilityRange; set => visibilityRange = value; }
    public bool CanSpawnUnits { get => canSpawnUnits; set => canSpawnUnits = value; }
    public bool CanTradeCards { get => canTradeCards; set => canTradeCards = value; }
    public bool ResourceGainsAllowed { get => resourceGainsAllowed; set => resourceGainsAllowed = value; }
    public int TradeCooldown { get => tradeCooldown; set => tradeCooldown = value; }

    private void OnEnable() {
        TurnManager.OnTurnStart += TurnStart;
        TurnManager.OnTurnEnd += TurnEnd;
    }

    private void OnDisable() {
        TurnManager.OnTurnStart -= TurnStart;
        TurnManager.OnTurnEnd -= TurnEnd;
    }

    void Start() {
        Tile = gameObject.GetComponent<Tile>();

        // Ticks required for capture is based on number of players:
        try {
            turnManager = GameObject.Find("TurnManager").GetComponent<TurnManager>();
            captureTicksRequired = turnManager.Players.Count;
        }
        catch(NullReferenceException e) {
            Debug.LogWarning(e);
        }

        SetupControlPointValues();
    }

    void SetupControlPointValues() {
        // Setup values based on control point SO:
        StandardResPerTurn = ControlPointSO.standardResPerTurn;
        SpecialResPerTurn = ControlPointSO.specialResPerTurn;
        VisibilityRange = ControlPointSO.visibilityRange;
        CanSpawnUnits = Tile.CanSpawnUnits = ControlPointSO.canSpawnUnits;
        CanTradeCards = ControlPointSO.canTradeCards;
        ResourceGainsAllowed = ControlPointSO.resourceGainsAllowed;
    }

    void OnOwnerChanged() {
        Tile.ObjectOnTop.GetComponent<MeshRenderer>().material.color = Owner.playerColor;
        Owner.ownedControlPoints.Add(this);
        Debug.Log("Control point captured!");
    }

    void TurnStart() {
        ApplyTurnStartEffects();
        LookForNearbyUnits();
        if (Owner != null && Owner == turnManager.CurrentPlayer) {
            // Decrease cooldown if needed:
            if (TradeCooldown >= 0) { 
                TradeCooldown--;
            }
            // Reset card trade ability when cooldown reaches 0:
            if (TradeCooldown == 0) { 
                CanTradeCards = controlPointSO.canTradeCards;
                MessageManager.Show("Cooldown has ended for your " + controlPointSO.controlPointName + "!");
            }


            // Increase visibility:
            Tile.IncreaseVisibilityInRange(VisibilityRange);

            // Add resources to the owner:
            if(ResourceGainsAllowed) {
                AddManaToPlayer(Owner);
            }
            else if(!ResourceGainsAllowed) {
                //MessageManager.Show("Resource gains have been denied for your " + controlPointSO.controlPointName + "!");
            }
        }
    }

    void TurnEnd() {
        ApplyTurnEndEffects();
        if (Owner != null && Owner == turnManager.CurrentPlayer) {
            Tile.DecreaseVisibilityInRange(VisibilityRange);
        }
    }
    void ApplyTurnStartEffects() {
        // Apply effects that activate at start of turn:
        if (effects.Count > 0) {
            for (int i = 0; i < effects.Count; i++) {
                switch (effects[i].effectSO.effectActivationType) {
                    case EffectSO.EffectActivationType.AnyTurnStart:
                    case EffectSO.EffectActivationType.AnyLastTurnStart:
                        effects[i].Activate(gameObject, gameObject, this);
                        break;
                    case EffectSO.EffectActivationType.CasterTurnStart:
                    case EffectSO.EffectActivationType.CasterLastTurnStart:
                        if (turnManager.CurrentPlayer == effects[i].Caster)
                            effects[i].Activate(gameObject, gameObject, this);
                        break;
                    case EffectSO.EffectActivationType.EnemyTurnStart:
                    case EffectSO.EffectActivationType.EnemyLastTurnStart:
                        if (turnManager.CurrentPlayer != effects[i].Caster)
                            effects[i].Activate(gameObject, gameObject, this);
                        break;
                    case EffectSO.EffectActivationType.OwnerTurnStart:
                    case EffectSO.EffectActivationType.OwnerLastTurnStart:
                        if (effects[i].Owner == turnManager.CurrentPlayer)
                            effects[i].Activate(gameObject, gameObject, this);
                        break;
                    default:
                        break;
                }
            }
        }
    }
    void ApplyTurnEndEffects() {
        // Apply effects that activate at the end of turn:
        if (effects.Count > 0) {
            for (int i = 0; i < effects.Count; i++) {
                switch (effects[i].effectSO.effectActivationType) {
                    case EffectSO.EffectActivationType.AnyTurnEnd:
                    case EffectSO.EffectActivationType.AnyLastTurnEnd:
                        effects[i].Activate(gameObject, gameObject, this);
                        break;
                    case EffectSO.EffectActivationType.CasterTurnEnd:
                    case EffectSO.EffectActivationType.CasterLastTurnEnd:
                        if (turnManager.CurrentPlayer == effects[i].Caster)
                            effects[i].Activate(gameObject, gameObject, this);
                        break;
                    case EffectSO.EffectActivationType.EnemyTurnEnd:
                    case EffectSO.EffectActivationType.EnemyLastTurnEnd:
                        if (turnManager.CurrentPlayer != effects[i].Caster)
                            effects[i].Activate(gameObject, gameObject, this);
                        break;
                    case EffectSO.EffectActivationType.OwnerTurnEnd:
                    case EffectSO.EffectActivationType.OwnerLastTurnEnd:
                        if (effects[i].Owner == turnManager.CurrentPlayer)
                            effects[i].Activate(gameObject, gameObject, this);
                        break;
                    default:
                        break;
                }
            }
        }
    }
    void AddManaToPlayer(Player player) {
        // Give resources to owner:
        if(StandardResPerTurn > 0) player.playerResources.AddMana(StandardResPerTurn);
        if(SpecialResPerTurn > 0) player.playerResources.AddSpecialMana(SpecialResPerTurn);

        // Display a message if resources are gained:
        string message = "";
        if (StandardResPerTurn > 0) message = "You gained +" + StandardResPerTurn + " St.Res ";
        if (SpecialResPerTurn > 0) message += "and +" + SpecialResPerTurn + " Sp.Res ";
        message += "from your " + controlPointSO.controlPointName + "!";
        if (StandardResPerTurn > 0 || SpecialResPerTurn > 0) MessageManager.Show(message);
    }
    void LookForNearbyUnits() {
        nearbyUnits = new List<Unit>();
        // Check each neighbour tile for units:
        foreach(GameObject tile in Tile.NeighbourTiles) {
            if(tile.GetComponent<Tile>().UnitOnTop != null) {
                nearbyUnits.Add(tile.GetComponent<Tile>().UnitOnTop.GetComponent<Unit>());
            }
        }
        // Reset capture counter if no units nearby:
        if (nearbyUnits.Count == 0) {
            playerCapturing = null;
            captureCounter = 0;
        }
        else if (nearbyUnits.Count > 0) {
            // Loop through nearby units:
            for (int i = 0; i < nearbyUnits.Count; i++) {
                // If the control point is already owned by someone:
                if (Owner != null) {
                    // If there's an enemy Locust Swarm nearby:
                    if (nearbyUnits[i].scriptableUnitCard.cardName == "Locust Swarm" && nearbyUnits[i].Owner != Owner) {
                        // Activate Locust Swarm ability:
                        Ability ability = nearbyUnits[i].GetAbilityByName("Locust Swarm");
                        nearbyUnits[i].TriggerAbility(ability, this.gameObject);
                    }
                }

                // Attempt capture if there's an enemy unit nearby:
                if (Owner != nearbyUnits[i].Owner) {
                    AttemptCapture();
                    break;
                }
            }
        }
    }
    void AttemptCapture() {
        Debug.Log("Attempting capture...");
        bool canBeCaptured = true;

        if (nearbyUnits.Count == 1) {
            // Units on top block capture attempts:
            if (Tile.UnitOnTop != null) {
                canBeCaptured = false;
            }
        }
        else if (nearbyUnits.Count > 1) {
            // Compare each pair of unit to see if they're owned by different players:
            for (int i = 0; i <= (nearbyUnits.Count - 1); i++) {
                for (int j = (i + 1); j < nearbyUnits.Count; j++) {
                    // Having units of different players block capture attempts:
                    if (nearbyUnits[i].Owner != nearbyUnits[j].Owner) {
                        canBeCaptured = false;
                        break;
                    }
                }
            }
        }

        // If the control point can be captured:
        if (canBeCaptured && nearbyUnits.Count > 0) {
            // If the control point is already owned by the unit owner:
            if(Owner == nearbyUnits[0].Owner) {
                return;
            }

            // If no one is currently trying to capture control point:
            if (playerCapturing == null) {
                // Start capturing control point:
                playerCapturing = nearbyUnits[0].Owner;
                captureCounter++;
            }
            // If someone is already capturing control point:
            else if(playerCapturing != null) {
                // Same player:
                if(playerCapturing == nearbyUnits[0].Owner) {
                    // Continue capturing control point:
                    captureCounter++;
                }
                // Different player:
                else {
                    // Reset capture:
                    playerCapturing = null;
                    captureCounter = 0;
                }
            }

            // Capture complete:
            if (captureCounter == captureTicksRequired) {
                // Check if there's a 'Cult of the Sleeping God' unit nearby:
                foreach(Unit unit in nearbyUnits) {
                    if (unit.scriptableUnitCard.cardName == "Cult of the Sleeping God") {
                        // Trigger the unit's ability (spawns minions but denies resource gains):
                        Ability ability = unit.GetAbilityByName("Cult of the Sleeping God");
                        unit.TriggerAbility(ability, this.gameObject);
                    }
                }

                // Change owner and reset variables:
                Owner = playerCapturing;
                playerCapturing = null;
                captureCounter = 0;
                MessageManager.Show("A " + controlPointSO.controlPointName + " has been captured by " + Owner.playerName + "!");
            }
        }
        else if (!canBeCaptured) {
            MessageManager.Show("Capture attempt has been blocked by another player!");
        }
        Debug.Log("Capture progress: " + captureCounter + "/" + captureTicksRequired);
    }
}
