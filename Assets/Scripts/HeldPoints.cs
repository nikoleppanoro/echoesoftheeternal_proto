﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class HeldPoints : MonoBehaviour {
    public TextMeshProUGUI controlPointsHeldText, towersHeldText;
    public HexTileMap hexTileMap;
    public TurnManager turnManager;

    [SerializeField] List<ControlPoint> controlPointsOnMap = new List<ControlPoint>();
    [SerializeField] List<ControlPoint> towersOnMap = new List<ControlPoint>();

    int ownedControlPointCount, ownedTowerCount;

    private void OnEnable() {
        TurnManager.OnTurnStart += TurnStart;
    }

    private void OnDisable() {
        TurnManager.OnTurnStart -= TurnStart;
    }

    void Start() {
        // Get the number of all control points and towers:
        foreach(GameObject tile in hexTileMap.tiles) {
            // If the tile a control point:
            if(hexTileMap.CheckIfControlPoint(tile.GetComponent<Tile>().ScriptableTile)) {
                controlPointsOnMap.Add(tile.GetComponent<ControlPoint>());
                // If the control point is a tower:
                if (hexTileMap.CheckIfTower(tile.GetComponent<Tile>().ScriptableTile)) {
                    towersOnMap.Add(tile.GetComponent<ControlPoint>());
                }
            }
        }
        UpdateHeldPointsDisplay();
    }

    void TurnStart() {
        GetControlPointCounts();
        UpdateHeldPointsDisplay();
    }

    void GetControlPointCounts() {
        // Control Points:
        ownedControlPointCount = turnManager.CurrentPlayer.ownedControlPoints.Count;
        // Towers: 
        ownedTowerCount = 0;
        foreach (ControlPoint cp in turnManager.CurrentPlayer.ownedControlPoints) {
            if(hexTileMap.CheckIfTower(cp.GetComponent<Tile>().ScriptableTile)) {
                ownedTowerCount++;
            }
        }
    }

    public void UpdateHeldPointsDisplay() {
        controlPointsHeldText.text = "Control points held: " + ownedControlPointCount + "/" + controlPointsOnMap.Count;
        towersHeldText.text = "Towers held: " + ownedTowerCount + "/" + towersOnMap.Count;
    }
}
