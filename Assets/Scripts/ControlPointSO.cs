﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "New Control Point", menuName = "ScriptableObjects/ControlPoint")]

public class ControlPointSO : ScriptableObject {
    [Header("Info")]
    public string controlPointName;

    [Header("Properties")]
    public int standardResPerTurn;
    public int specialResPerTurn;
    public int visibilityRange;
    public bool canSpawnUnits;
    public bool canTradeCards;
    public bool resourceGainsAllowed;
}
