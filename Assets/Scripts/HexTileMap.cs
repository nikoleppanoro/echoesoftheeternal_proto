﻿using System.Collections.Generic;
using UnityEngine;
using System.IO;
using UnityEditor;
using TMPro;
using System;

public class HexTileMap : MonoBehaviour
{
    [Header("Other")]
    public bool startInEditMode = false;
    public TurnManager turnManager;

    [Header("Map properties")]
    public string mapFileName;
    public TMP_InputField mapNameInput;
    [SerializeField] int mapSizeX = 10;
    [SerializeField] int mapSizeY = 10;
    [SerializeField] float tileGap = 0f;
    [SerializeField] float xOffset = 1f;
    [SerializeField] float zOffset = 0.86553f;

    [Header("Tile")]
    public Transform tileParent;
    public GameObject tilePrefab;
    public List<TileSO> scriptableObjectTiles;
    public List<ControlPointSO> scriptableObjectControlPoints;
    public List<Material> tileColors;
    public List<GameObject> tiles;
    public List<Tile> tilesInAttackRange;
    public List<Tile> tilesInMovementRange;

    [Header("Fog Of War")]
    [SerializeField] bool fogOfWarEnabled;  // Reduced visibility and black fog
    [SerializeField] bool darknessEnabled;  // Reduced visibility but no black fog
    public GameObject fogTilePrefab;
    public GameObject fogTileParent;
    public List<GameObject> fogTiles;

    public int MapSizeX { get => mapSizeX; set => mapSizeX = value; }
    public int MapSizeY { get => mapSizeY; set => mapSizeY = value; }
    public float TileGap { get => tileGap; set => tileGap = value; }
    public float XOffset { get => xOffset; set => xOffset = value; }
    public float ZOffset { get => zOffset; set => zOffset = value; }
    public bool FogOfWarEnabled {get => fogOfWarEnabled; set => fogOfWarEnabled = value; }
    public bool DarknessEnabled { get => darknessEnabled; set => darknessEnabled = value; }

    void Start() {
        if (mapFileName != "") {
            Debug.Log("Loading map '" + mapFileName + "'...");
            LoadMap();
        }
        else {
            Debug.Log("Creating new map...");
            CreateNewMap();
        }

        if (FogOfWarEnabled && !startInEditMode) {
            CreateFogOfWar();
        }
    }
    private void OnEnable() {
        if(FogOfWarEnabled) {
            TurnManager.OnTurnStart += TurnStart;
            TurnManager.OnTurnEnd += TurnEnd;
        }
    }
    private void OnDisable() {
        if (FogOfWarEnabled) {
            TurnManager.OnTurnStart -= TurnStart;
            TurnManager.OnTurnEnd -= TurnEnd;
        }
    }
    private void CreateNewMap() {
        for (int y = 0; y < MapSizeY; y++) {
            for (int x = 0; x < MapSizeX; x++) {
                // Calculate xPos and zPos:
                float xPos = (x * XOffset) + (x * TileGap);
                float zPos = (y * ZOffset) + (y * TileGap);

                // Change xPos if we're on an odd row:
                if (y % 2 == 1) {
                    xPos += XOffset / 2;
                }

                //Instantiate tile and set parent:
                GameObject obj = Instantiate(tilePrefab, new Vector3(xPos, 0, zPos), Quaternion.identity, tileParent);
                Tile tile = obj.GetComponent<Tile>();
                //Rename tile and set values:
                if (y % 2 == 1) {
                    obj.name = "Hex_" + (x * 2 + 1) + "_" + y;
                    tile.doubleWidthCoordinateX = (x * 2 + 1);
                    tile.doubleWidthCoordinateY = y;
                }
                else {
                    obj.name = "Hex_" + (x * 2) + "_" + y;
                    tile.doubleWidthCoordinateX = (x * 2);
                    tile.doubleWidthCoordinateY = y;
                }
                tile.ScriptableTile = GetTileByName("Grassland"); ; // Grassland tile

                tiles.Add(obj);
            }
        }
        // Get neighbours:
        foreach(GameObject tile in tiles) {
            tile.GetComponent<Tile>().enabled = true;
            tile.GetComponent<Tile>().GetNeighbours();
        }
    }
    public void LoadMap() {
        string tileName;
        int tileHeight;

        mapNameInput.text = mapFileName;

        //Path to map file:
        string path = Path.Combine(Application.streamingAssetsPath, "Maps/" + mapFileName + ".bytes");
        Debug.Log("Path: " + path);

        // Create stream for reading:
        FileStream stream = new FileStream(path, FileMode.Open);

        using (BinaryReader reader = new BinaryReader(stream)) {
            // The map file should consist of:
            // - mapSizeX and mapSizeY (int)
            // after that, for each tile on the map:
            // - tileName (string) 
            // - tileHeight (int) 

            // Read map size:
            MapSizeX = reader.ReadInt32();
            MapSizeY = reader.ReadInt32();

            for (int y = 0; y < MapSizeY; y++) {
                for (int x = 0; x < MapSizeX; x++) {
                    // Calculate xPos and zPos:
                    float xPos = (x * XOffset) + (x * TileGap);
                    float zPos = (y * ZOffset) + (y * TileGap);

                    // Change xPos if we're on an odd row:
                    if (y % 2 == 1) {
                        xPos += XOffset / 2;
                    }

                    // Read tile name from file:
                    tileName = reader.ReadString();

                    // Instantiate a new tile:
                    GameObject obj = Instantiate(tilePrefab, new Vector3(xPos, 0, zPos), Quaternion.identity, tileParent);
                    Tile tile = obj.GetComponent<Tile>();

                    // Assign tile type to tile:
                    TileSO scriptableTile = GetTileByName(tileName);
                    tile.ScriptableTile = scriptableTile;

                    // Add ControlPoint script to tile if it is a control point tile:
                    if (CheckIfControlPoint(scriptableTile)) {
                        ControlPoint cp = obj.AddComponent<ControlPoint>();
                        cp.ControlPointSO = GetControlPointByName(scriptableTile.name);
                    }

                    // Rename tile and set values:
                    if (y % 2 == 1) {
                        obj.name = "Hex_" + (x * 2 + 1) + "_" + y;
                        // Doublewidth coordinates:
                        tile.doubleWidthCoordinateX = (x * 2 + 1);
                        tile.doubleWidthCoordinateY = y;
                    }
                    else {
                        obj.name = "Hex_" + (x * 2) + "_" + y;
                        // Doublewidth coordinates:
                        tile.doubleWidthCoordinateX = (x * 2);
                        tile.doubleWidthCoordinateY = y;
                    }

                    // Offset coordinates:
                    tile.offsetCoordinateX = x;
                    tile.offsetCoordinateY = y;

                    // Cubed coordinates: 
                    tile.cubeCoordinateX = x - (y - (y&1)) / 2;
                    tile.cubeCoordinateZ = y;
                    tile.cubeCoordinateY = -tile.cubeCoordinateX - tile.cubeCoordinateZ;

                    // Read tile height:
                    tileHeight = reader.ReadInt32();
                    tile.Height = tileHeight;

                    tiles.Add(obj);
                }
            }
        }

        // Activate script, get tile neighbours:
        foreach (GameObject tile in tiles) {
            tile.GetComponent<Tile>().enabled = true;
            tile.GetComponent<Tile>().GetNeighbours();
        }
    }
    public void SaveMap() {
        if(mapNameInput.text == "") {
            MessageManager.Show("Please enter a name for the map before you try to save it!");
        }
        else {
            string path = Path.Combine(Application.streamingAssetsPath, "Maps/" + mapNameInput.text + ".bytes");
            BinaryWriter writer = new BinaryWriter(File.Open(path, FileMode.Create));

            writer.Write(MapSizeX);
            writer.Write(MapSizeY);

            // Loop through each tile on map and save data:
            for (int i = 0; i < tiles.Count; i++) {
                writer.Write(tiles[i].GetComponent<Tile>().ScriptableTile.name);
                writer.Write(tiles[i].GetComponent<Tile>().Height);
            }
            writer.Close();
            Debug.Log("Map " + mapNameInput.text + " was saved to " + path + "!");
            MessageManager.Show("Map " + mapNameInput.text + " was saved to " + path + "!");
        }
    }
    private TileSO GetTileByName(string name) {
        try {
            return scriptableObjectTiles.Find(x => x.name == name);
        }
        catch(Exception e) {
            Debug.LogWarning("Tile could not be found from the list: " +  e);
            return scriptableObjectTiles[0];
        }
    }
    private ControlPointSO GetControlPointByName(string name) {
        try {
            return scriptableObjectControlPoints.Find(x => x.name == name);
        }
        catch (Exception e) {
            Debug.LogWarning("Tile could not be found from the list: " + e);
            return scriptableObjectControlPoints[0];
        }
    }
    public bool CheckIfControlPoint(TileSO tile) {
        try {
            if (tile.name.Contains("CP")) return true;
            else return false;
        }
        catch (NullReferenceException e) {
            Debug.LogWarning(e);
        }
        return false;
    }
    public bool CheckIfTower(TileSO tile) {
        try {
            if (tile.name.Contains("Visibility")) return true;
            else return false;
        }
        catch (NullReferenceException e) {
            Debug.LogWarning(e);
        }
        return false;
    }

    public void ResetTileDistances() {
        // Set distance of all tiles to max to show we haven't visited it yet:
        foreach (GameObject tile in tiles) {
            tile.GetComponent<Tile>().Distance = int.MaxValue;
        }
        tilesInMovementRange = new List<Tile>();
    }
    public void ResetAttackRange() {
        foreach(Tile tile in tilesInAttackRange) {
            tile.IsInAttackRange = false;
        }
        tilesInAttackRange = new List<Tile>();
    }
    public void UpdateTileDistances() {
        if(tilesInMovementRange.Count > 0) {
            foreach (Tile tile in tilesInMovementRange) {
                float distance = tile.GetComponent<Tile>().Distance;
                if (distance != int.MaxValue || distance != 0) {
                    tile.UpdateDistanceText();
                }
            }
        }
    }
    void CreateFogOfWar() {
        for (int y = 0; y < MapSizeY; y++) {
            for (int x = 0; x < MapSizeX; x++) {

                // Calculate xPos and zPos:
                float xPos = (x * XOffset) + (x * TileGap);
                float zPos = (y * ZOffset) + (y * TileGap);

                // Change xPos if we're on an odd row:
                if (y % 2 == 1) {
                    xPos += XOffset / 2;
                }

                //Instantiate tile and set parent:
                GameObject obj = Instantiate(fogTilePrefab, new Vector3(xPos, 0, zPos), Quaternion.identity, fogTileParent.transform);

                //Rename tile and set values:
                if (y % 2 == 1) {
                    obj.name = "Fog_" + (x * 2 + 1) + "_" + y;
                }
                else {
                    obj.name = "Fog_" + (x * 2) + "_" + y;
                }

                // Assign fog tile to tile:
                tiles[(MapSizeY * y) + x].GetComponent<Tile>().FogTile = obj;

                //Rescale fog tile:
                obj.transform.localScale = new Vector3(1, 2, 1);

                // Remove collider (to avoid raycasts from getting blocked): 
                obj.GetComponent<Collider>().enabled = false;

                fogTiles.Add(obj);
            }
        }
    }
    void TurnStart() {
        // Display current players tiles:
        foreach (GameObject tile in turnManager.CurrentPlayer.exploredTiles) {
            tile.GetComponent<Tile>().HasBeenExplored = true;
        }
    }
    void TurnEnd() {
        // Hide all tiles by marking tiles unexplored:
        foreach (GameObject tile in tiles) {
            tile.GetComponent<Tile>().HasBeenExplored = false;
        }
    }
}
